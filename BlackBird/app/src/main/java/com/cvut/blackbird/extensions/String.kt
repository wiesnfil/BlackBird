package com.cvut.blackbird.extensions

import android.util.Base64

fun String.base64Encoded(): String = Base64.encodeToString(toByteArray(), Base64.NO_WRAP)
fun String.lastIndexOfNumber(): Int = indexOfLast {it.isDigit()} + 1

val String.courseAbbr get() = this.substring(
        this.lastIndexOfNumber()
                .coerceAtMost(this.length - 3)
                .coerceAtLeast(0),
        this.length
)