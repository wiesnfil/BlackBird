package com.cvut.blackbird.extensions

import android.content.res.Resources

val density = Resources.getSystem().displayMetrics.density

val Int.dpToPx: Int get() = (this * density).toInt()
val Float.dpToPx: Float get() = this * density

fun Long.millisToTimeText() = buildString {
    val time = this@millisToTimeText / 1000 / 60

    append("in ")
    if (time < 60) // Less than a hour
        append(time.toString() + " min")
    else if (time < 1440) { // Less than a day
        if (time / 60 > 0) append((time / 60).toString() + " h ")

        if (time % 60 > 0) append((time % 60).toString() + " min")
        else delete(length - 1, length)
    } else {
        append((time / 1440).toString() + " d ")
        if ((time % 1440) / 60 > 0) append(((time % 1440) / 60).toString() + " h")
        else delete(length - 1, length)
    }
}
