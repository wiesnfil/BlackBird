package com.cvut.blackbird.extensions

import androidx.fragment.app.Fragment
import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService



fun Fragment.closeKeyboard() {
    val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    val view = activity?.currentFocus ?: View(activity)

    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.getColor(id: Int) = ContextCompat.getColor(requireContext(),id)