package com.cvut.blackbird.model.services.kos

import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.database.EventDao
import com.cvut.blackbird.model.database.TeacherDao
import com.cvut.blackbird.model.database.getTeachers
import com.cvut.blackbird.model.entities.Teacher
import com.cvut.blackbird.model.services.AuthService
import com.cvut.blackbird.model.services.BBMicroService
import com.cvut.blackbird.model.services.KosService
import com.cvut.blackbird.model.services.evaluateResult
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async


class LoadTeachers(
        override val authService: AuthService,
        private val kosService: KosService,
        private val eventDao: EventDao,
        private val teacherDao: TeacherDao
) : BBMicroService() {

    suspend operator fun invoke(): Result<Unit> {
        val deferredTeacherList = ArrayList<Deferred<Result<Teacher>>>()
        val completedTeacherList = ArrayList<Result<Teacher>>()

        val teachers = eventDao.getTeachers()
        for (teacher in teachers)
            deferredTeacherList.add(GlobalScope.async { fetch { kosService.getTeacher(teacher) } })
        deferredTeacherList.forEach { completedTeacherList.add(it.await()) }

        val result = completedTeacherList.evaluateResult()
        if (result is Success) teacherDao.insertAll(completedTeacherList.map { (it as Success).value })
        return result
    }
}