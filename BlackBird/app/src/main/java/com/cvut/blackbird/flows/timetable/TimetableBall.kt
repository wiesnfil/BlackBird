package com.cvut.blackbird.flows.timetable

import android.content.Context
import android.util.TypedValue
import android.view.Gravity
import android.widget.FrameLayout
import androidx.core.view.doOnLayout
import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.*
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.entities.EventType
import com.cvut.blackbird.support.kolor.Kolor
import com.cvut.blackbird.support.wobbly.WobblyElement
import com.github.florent37.kotlin.pleaseanimate.please

class TimetableBall(context: Context): WobblyElement(context) {
    lateinit var payload: Event

    override fun setupLayout(data: Any?) {
        data as Event
        payload = data
        background = when {
            data.eventType == EventType.LECTURE  -> context.resources.getDrawable(R.drawable.ic_timetable_lecture, context.theme)
            data.eventType == EventType.TUTORIAL -> context.resources.getDrawable(R.drawable.ic_timetable_tutorial, context.theme)
            else                                 -> context.resources.getDrawable(R.drawable.ic_timetable_info, context.theme)
        }

        val name = data.linked?.course ?: "-"
        this.text = name.substring(
                name.lastIndexOfNumber()
                        .coerceAtMost(name.length - 3)
                        .coerceAtLeast(0),
                name.length)

        if (text == "") text = "?"
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 17f)

//        backgroundTintList = ColorStateList
//                .valueOf(Kolor.fromSeed(firstName.hashCode())
//                .withBrightness(0.75f)
//                .withSaturation(0.65f)
//                .colorInt)


        layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT)
                .apply {
                    height = 60.dpToPx
                    width = 60.dpToPx
                }
        gravity = Gravity.CENTER
    }
}