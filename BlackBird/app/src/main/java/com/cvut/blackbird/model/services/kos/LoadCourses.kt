package com.cvut.blackbird.model.services.kos

import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.database.CourseDao
import com.cvut.blackbird.model.entities.Course
import com.cvut.blackbird.model.services.AuthService
import com.cvut.blackbird.model.services.BBMicroService
import com.cvut.blackbird.model.services.KosService
import com.cvut.blackbird.model.services.evaluateResult
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class LoadCourses(
        override val authService: AuthService,
        private val kosService: KosService,
        private val courseDao: CourseDao
) : BBMicroService() {

    suspend operator fun invoke(): Result<Unit> {
        val deferredCourseList = ArrayList<Deferred<Result<Course>>>()
        val completedCourses = ArrayList<Result<Course>>()

        val courses = fetch { kosService.getStudentsCourses() }
        if (courses is Success) {
            for (link in courses.value.courses)
                deferredCourseList.add(GlobalScope.async { fetch { kosService.getCourse(link.name) } })
            deferredCourseList.forEach { completedCourses.add(it.await()) }
        } else return Failure((courses as Failure).message)

        val result = completedCourses.evaluateResult()
        if (result is Success) courseDao.insertAll(completedCourses.map { (it as Success).value })
        return result
    }
}