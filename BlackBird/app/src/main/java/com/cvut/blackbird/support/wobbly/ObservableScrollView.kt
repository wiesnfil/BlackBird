package com.cvut.blackbird.support.wobbly

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.Button
import android.widget.HorizontalScrollView

class ObservableScrollView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : HorizontalScrollView(context, attrs, defStyle) {

    private var mIsScrolling: Boolean = false
    private var mIsTouching: Boolean = false
    private var mScrollingRunnable: Runnable? = null
    var onScrollListener: OnScrollListener? = null


    interface OnScrollListener {
        fun onScrollChanged(scrollView: ObservableScrollView, x: Int, y: Int, oldX: Int, oldY: Int)
        fun onEndScroll(scrollView: ObservableScrollView)
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        val action = ev.action

        if (action == MotionEvent.ACTION_MOVE) {
            mIsTouching = true
            mIsScrolling = true
        } else if (action == MotionEvent.ACTION_UP) {
            if (mIsTouching && !mIsScrolling) {
                if (onScrollListener != null) {
                    onScrollListener!!.onEndScroll(this)
                }
            }

            mIsTouching = false
        }

        return super.onTouchEvent(ev)
    }

    override fun onScrollChanged(x: Int, y: Int, oldX: Int, oldY: Int) {
        super.onScrollChanged(x, y, oldX, oldY)

        if (Math.abs(oldX - x) > 0) {
            if (mScrollingRunnable != null) {
                removeCallbacks(mScrollingRunnable)
            }

            mScrollingRunnable = Runnable {
                if (mIsScrolling && !mIsTouching) {
                    if (onScrollListener != null) {
                        onScrollListener!!.onEndScroll(this@ObservableScrollView)
                    }
                }

                mIsScrolling = false
                mScrollingRunnable = null
            }

            postDelayed(mScrollingRunnable, 200)
        }

        onScrollListener?.onScrollChanged(this, x, y, oldX, oldY)
    }
}