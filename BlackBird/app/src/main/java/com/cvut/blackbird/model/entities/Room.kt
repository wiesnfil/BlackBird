package com.cvut.blackbird.model.entities

import com.cvut.blackbird.model.KosEntry
import com.cvut.blackbird.model.KosPage
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml(inheritance = true)
class RoomPage : KosPage<Room>()

@Xml(inheritance = true)
data class Room(
        @PropertyElement val code: String,
        @PropertyElement val division: String,
        @PropertyElement val locality: String,
        @PropertyElement val name: String,
        @PropertyElement val type: String,

        @PropertyElement val capacity: String
): KosEntry<Room>()