package com.cvut.blackbird.flows.authentication

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.appendParameters
import com.cvut.blackbird.extensions.buildUri
import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.services.AuthService
import com.cvut.blackbird.model.services.UserInfo
import com.cvut.blackbird.model.support.AuthResult
import com.cvut.blackbird.support.glue.bind
import com.cvut.blackbird.support.glue.observe
import com.cvut.blackbird.support.glue.toPassValueTo
import com.cvut.blackbird.support.glue.visibilityTo
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.auth_fragment.*

class AuthFragment : Fragment() {
    private lateinit var viewModel: AuthViewModel
    private var snack: Snackbar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.auth_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        setupUi()
        setupBinding()
    }


    private fun setupUi() {
        snack = Snackbar
                .make(view!!, "", Snackbar.LENGTH_INDEFINITE)
                .setAction("Try Again") { viewModel.refreshToken() }
    }

    private fun setupBinding() {

        observe(viewModel.userInitResult) { result ->
            infoMessage.text = when (result) {
                is Success -> {
                    onAuthFinished()
                    "Successfully fetched"
                }

                is Failure -> {
                    UserInfo.username = ""
                    result.message
                }

                else -> "Loading"
            }
        }

        observe(viewModel.authStatus) { result ->
            if (result == AuthResult.SUCCESS)
                viewModel.initUser()
            else if (result != AuthResult.NOT_YET) {
                snack?.setText("Error: ${result.name}")
                infoMessage.text = result.errorDesc
                if (!result.critical) authorize()
                else snack!!.show()
            }
        }

        observe(viewModel.authLoadingStatus) { if (it) snack?.dismiss() }
        bind(authProgress) visibilityTo viewModel.authLoadingStatus
        bind(viewModel.userInitState) toPassValueTo infoMessage::setText
        
    }


    /**
     * Auth processes handling
     */

    private fun onAuthFinished() {
        view!!.findNavController().navigate(R.id.onLogged)
    }

    override fun onResume() {
        super.onResume()
        if (activity?.intent?.data != null) {
            getAccessToken(activity!!.intent!!.data!!)
        } else {
            authorize()
        }
    }

    private fun getAccessToken(data: Uri) {
        val response = Uri.parse(data.toString())
        if (response.queryParameterNames.contains("code"))
            viewModel.initToken(response.getQueryParameter("code") ?: "")
        else throw Throwable("Code was not returned when trying to authenticate")
    }

    //    Authorization
    private fun authorize() {
        val uri = buildUri("https://auth.fit.cvut.cz/oauth/authorize") {
            appendParameters(hashMapOf(
                    "response_type" to "code",
                    "client_id" to AuthService.clientId,
                    "redirect_uri" to "kosapp://callback"
            ))
        }

        val intent = Intent(Intent.ACTION_VIEW, uri)
        if (activity != null && intent.resolveActivity(activity!!.packageManager) != null) {
            startActivity(intent)
        }
    }
}