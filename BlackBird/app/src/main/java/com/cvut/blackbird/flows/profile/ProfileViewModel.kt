package com.cvut.blackbird.flows.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cvut.blackbird.extensions.JobStrategy
import com.cvut.blackbird.extensions.asProgressStatus
import com.cvut.blackbird.extensions.doWith
import com.cvut.blackbird.extensions.withDefault
import com.cvut.blackbird.flows.BlackBirdVM
import com.cvut.blackbird.model.services.KosParameters
import com.cvut.blackbird.model.services.KosService
import com.cvut.blackbird.model.services.kos.LoadRooms
import com.cvut.blackbird.model.support.NotYet
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.services.local.WipeData
import com.cvut.blackbird.model.support.map
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class ProfileViewModel : BlackBirdVM() {
    private val loadRooms by instance<LoadRooms>()
    private val wipeData by instance<WipeData>()

    private val _logoutStatus = MutableLiveData<Result<Unit>>() withDefault NotYet()
//    private val _rooms = MutableLiveData<Result<List<Room>>>() withDefault NotYet()

    val logoutStatus: LiveData<Result<Unit>> get() = _logoutStatus
//    val rooms: LiveData<Result<List<Room>>> get() = _rooms

    init {
        launch {
//            _rooms.postValue(loadRooms().map {
//                it.entries.map { it.rawContent as Room }
//            })
        }
    }

    var logoutJob: Job? = null
    fun logOut() {
        logoutJob = doWith(logoutJob, JobStrategy.EXIT) {
            launch { _logoutStatus asProgressStatus {
                wipeData()
                Success(Unit)
            } }
        }
    }
}
