package com.cvut.blackbird.extensions

import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.ViewTreeObserver

import org.jetbrains.anko.childrenSequence


var View.centerX : Float
    get() = x + (width/2)
    set(value) {
        x = value - (width/2)
    }

var View.centerY : Float
    get() = y + (height/2)
    set(value) {
        y = value - (height/2)
    }

val coords = IntArray(2)
var View.globalX: Int
    get() {
        getLocationOnScreen(coords)
        return coords[0]
    }
    set(value) {
        getLocationOnScreen(coords)
        x = value - (coords[0] - x)
    }

var View.globalY: Int
    get() {
        getLocationOnScreen(coords)
        return coords[1]
    }
    set(value) {
        getLocationOnScreen(coords)
        y = value - (coords[1] - y)
    }

var View.globalCenterX : Int
    get() = globalX + (width/2)
    set(value) {
        globalX = value - (width/2)
    }

var View.globalCenterY : Int
    get() = globalY + (height/2)
    set(value) {
        globalY = value - (height/2)
    }

inline fun ViewGroup.removeViewsMatching(crossinline predicate: (View) -> Boolean) {
    childrenSequence()
            .filter { predicate(it) }
            .toList() // To prevent "Concurrent Modification Exception"
            .forEach (::removeView)
}

fun View.setVisible() {visibility = VISIBLE}
fun View.setInvisible() {visibility = INVISIBLE}
fun View.setGone() {visibility = GONE}


inline fun View.doOnGlobalLayout(crossinline action: (View) -> Unit) {
    val viewGlobalObserver  = object: ViewTreeObserver.OnGlobalLayoutListener { override fun onGlobalLayout() {
        action(this@doOnGlobalLayout)
        viewTreeObserver.removeOnGlobalLayoutListener(this)
    } }
    viewTreeObserver.addOnGlobalLayoutListener(viewGlobalObserver)
}
inline fun View.doOnLayoutChange (crossinline action: (View) -> Unit) {
    addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
        override fun onLayoutChange(v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
            v.removeOnLayoutChangeListener(this)
            action(v)
        }
    })
}

