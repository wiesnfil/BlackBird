package com.cvut.blackbird.model.services.other

import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.entities.News
import com.cvut.blackbird.model.entities.NewsRoot
import com.tickaroo.tikxml.TikXml
import okhttp3.OkHttpClient
import okhttp3.Request

class FetchNews {

    suspend operator fun invoke(): Result<List<News>> {
        val request = Request.Builder()
                .url(NewsRoot.getNewsLink())
                .addHeader("charset", "utf-10")
                .build()

        return try {
            val response = OkHttpClient().newCall(request).execute()
            if (response.isSuccessful) {
                Success(TikXml.Builder()
                        .exceptionOnUnreadXml(false)
                        .build()
                        .read(response.body()?.source(), NewsRoot::class.java)
                        .news)
            } else Failure(response.message())
        } catch (e: Exception) {
            Failure(e.localizedMessage)
        }
    }
}