package com.cvut.blackbird.extensions

fun StringBuilder.deleteLastLine() {
    delete(lastIndexOf("\n"),length)
}