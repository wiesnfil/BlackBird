package com.cvut.blackbird.model

object Constants {
    const val FACULTY_FEL = "fel"
    const val FACULTY_FIT = "fit"
}