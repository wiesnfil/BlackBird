package com.cvut.blackbird.model.services.kos

import com.cvut.blackbird.model.KosEntry
import com.cvut.blackbird.model.KosPage
import com.cvut.blackbird.model.services.AuthService
import com.cvut.blackbird.model.services.BBMicroService
import com.cvut.blackbird.model.services.KosService
import com.cvut.blackbird.model.support.Result

class LoadRooms(
        override val authService: AuthService,
        private val kosService: KosService
) : BBMicroService() {

    suspend operator fun invoke(): Result<KosPage<KosEntry<Any>>> = fetch { kosService.getRooms("") }
}