package com.cvut.blackbird.model.entities

import com.google.gson.annotations.SerializedName

data class AccessToken(
        @SerializedName("access_token")  val token: String,
        @SerializedName("refresh_token") val refreshToken: String?,
        @SerializedName("token_type")    val tokenType: String,
        @SerializedName("expires_in")    val expiresIn: Int
)

class User(
        @SerializedName("username") val username: String,
        @SerializedName("email")    val email: String
)