package com.cvut.blackbird.model.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.cvut.blackbird.model.KosPage
import com.tickaroo.tikxml.annotation.*

@Xml(inheritance = true)
class EnrolledCourseRoot (
        @Element(name = "atom:entry")
        val courses: List<EnrolledCourse>
)
@Xml
class EnrolledCourse (
        @Path("atom:content/course")
        @Attribute(name = "xlink:href")
        val link: String
) {
    val name get() = link.split('/')[1]
}

@Xml
@Entity
data class Course (
        @PrimaryKey
        @Path("atom:content")
        @PropertyElement val code: String,

        @Path("atom:content")
        @PropertyElement val completion: String,

        @Path("atom:content")
        @PropertyElement val credits: Int,

        @Path("atom:content")
        @PropertyElement val department: String,

        @Path("atom:content")
        @PropertyElement val homepage: String?,

        @Path("atom:content")
        @PropertyElement val name: String,

        @Path("atom:content")
        @PropertyElement val range: String,

        @Path("atom:content")
        @PropertyElement val season: String,

        @Path("atom:content")
        @PropertyElement val state: String,

        @Path("atom:content")
        @PropertyElement val studyForm: String?
) {
    companion object {
        fun empty() = Course("", "", -1, "", "", "", "", "", "", "")
    }
}