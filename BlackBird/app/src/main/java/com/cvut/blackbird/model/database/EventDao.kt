package com.cvut.blackbird.model.database

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.cvut.blackbird.model.entities.DetailedEvent
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.entities.EventNote
import org.joda.time.DateTime

@Dao
interface EventDao {

    /**
     * EVENT
     */

    @Query("SELECT * FROM Event")
    fun allEvents(): LiveData<List<Event>>

    @Query("SELECT * FROM Event WHERE eventTypeRaw = 'exam' AND startsAt > :now ORDER BY startsAt")
    fun futureExams(now: Long = DateTime.now().millis): LiveData<List<Event>>

    @Query("SELECT * FROM Event Where startsAt BETWEEN :from AND :to ORDER BY startsAt")
    fun timeSpan(from: Long, to: Long): LiveData<List<Event>>

    @Query("SELECT DISTINCT teachers FROM Event")
    fun getRawTeachers(): List<String>

    @Query("SELECT * FROM Event WHERE id = :id")
    fun getEvent(id: Int): Event?

    @Query("SELECT * FROM Event WHERE id IN (:ids)")
    fun getEvents(ids: List<Int>): List<Event>

    @Update
    fun updateEvent(event: Event)

    @Insert(onConflict = REPLACE)
    fun insertAllEvents(events: List<Event>)

    @Query("DELETE FROM Event")
    fun deleteAllEvents()

    @Transaction
    @Query("SELECT * FROM Event WHERE startsAt > :now ORDER BY startsAt LIMIT 1")
    fun nextEvent(now: Long = DateTime.now().millis): DetailedEvent?

    /** DETAILED EVENT */

    @Transaction
    @Query("SELECT * FROM Event WHERE id = :id")
    fun getDetailedEvent(id: Int): DetailedEvent

    @Transaction
    @Query("SELECT * FROM Event WHERE id IN (:ids)")
    fun getDetailedEvents(ids: List<Int>): LiveData<List<DetailedEvent>>

    /** EVENT - NOTE*/

    @Insert(onConflict = REPLACE)
    fun insertAllNotes(notes: List<EventNote>)

    @Insert(onConflict = REPLACE)
    fun saveEventNote(eventNote: EventNote)

    @Query("DELETE FROM EventNote")
    fun deleteAllNotes()
}

fun EventDao.insertAll(events: List<Event>) {
    insertAllNotes(events.map { EventNote(it.id) })
    insertAllEvents(events)
}

fun EventDao.getTeachers(): List<String> {
    val raw = getRawTeachers()
    val convertor = DBTypeConverters()
    val result = ArrayList<String>()

    for (json in raw) result.addAll(convertor.restoreList(json))

    return result.distinct()
}