package com.cvut.blackbird.flows.profile

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cvut.blackbird.BlackBirdAC

import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.toNotifyOnSuccess
import com.cvut.blackbird.flows.findMainNav
import com.cvut.blackbird.model.services.UserInfo
import com.cvut.blackbird.support.glue.bind
import com.cvut.blackbird.support.glue.clickTo
import com.cvut.blackbird.support.glue.observe
import kotlinx.android.synthetic.main.profile_fragment.*

class ProfileFragment : Fragment() {

    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.profile_fragment, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(ProfileViewModel::class.java)

        setupUi()
        setupBinding()
    }

    private fun setupUi() {
        profileFullName.text = "${UserInfo.firstName} ${UserInfo.surname}"
    }

    private fun setupBinding() {
        this bind logOutBtn clickTo viewModel::logOut
        this bind viewModel.logoutStatus toNotifyOnSuccess ::loggedOut

//        observe(viewModel.rooms) {
//            BlackBirdAC.log(it.toString())
//        }
    }

    private fun loggedOut() { findMainNav()?.navigate(R.id.toAuth) }
}
