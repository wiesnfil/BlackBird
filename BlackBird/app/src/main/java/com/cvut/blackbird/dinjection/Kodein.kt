package com.cvut.blackbird.dinjection

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.cvut.blackbird.model.database.BlackBirdDB
import com.cvut.blackbird.model.database.CourseDao
import com.cvut.blackbird.model.database.EventDao
import com.cvut.blackbird.model.database.TeacherDao
import com.cvut.blackbird.model.services.AuthService
import com.cvut.blackbird.model.services.KosService
import com.cvut.blackbird.model.services.SiriusService
import com.cvut.blackbird.model.services.auth.InitToken
import com.cvut.blackbird.model.services.auth.RefreshTokenGuarded
import com.cvut.blackbird.model.services.kos.*
import com.cvut.blackbird.model.services.local.WipeData
import com.cvut.blackbird.model.services.other.FetchNews
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import com.tickaroo.tikxml.TikXml
import com.tickaroo.tikxml.retrofit.TikXmlConverterFactory
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun Application.setupDI(): Kodein = Kodein {
    bind<Context>() with provider { this@setupDI.applicationContext }

    setupDB(this@setupDI)
    setupServices()
    setupMicroServices()
}

/** Database */
private fun Kodein.MainBuilder.setupDB(context: Context) {
    bind<BlackBirdDB>() with singleton { Room
            .databaseBuilder(context, BlackBirdDB::class.java, "blackbird_DB")
            .fallbackToDestructiveMigration()
            .build() }
    bind<EventDao>() with singleton { instance<BlackBirdDB>().eventDao() }
    bind<CourseDao>() with singleton { instance<BlackBirdDB>().courseDao() }
    bind<TeacherDao>() with singleton { instance<BlackBirdDB>().teacherDao() }
}


/** SERVICES */
private fun Kodein.MainBuilder.setupServices() {

    bind<AuthService>() with singleton { Retrofit.Builder().apply {
        baseUrl(AuthService.url)
        addConverterFactory(GsonConverterFactory.create())
    }.build().create(AuthService::class.java) }

    bind<KosService>() with singleton { Retrofit.Builder().apply {
        baseUrl(KosService.url)
        addConverterFactory(TikXmlConverterFactory.create(TikXml.Builder()
                .exceptionOnUnreadXml(false)
                .build()))
    }.build().create(KosService::class.java) }

    bind<SiriusService>() with singleton { Retrofit.Builder().apply {
        val dateTimeSer: JsonSerializer<DateTime> = JsonSerializer { src, _, _ ->
            if (src == null) null else JsonPrimitive(src.millis)
        }
        val dateTimeDeser: JsonDeserializer<DateTime> = JsonDeserializer<DateTime> { json, _, _ ->
            if (json == null) null
            else try { ISODateTimeFormat.dateTime().parseDateTime(json.asString) }
                catch (e: UnsupportedOperationException) { DateTime(json.asLong) }
        }

        baseUrl(SiriusService.url)
        addConverterFactory(GsonConverterFactory.create(
                GsonBuilder()
                        .registerTypeAdapter(DateTime::class.java, dateTimeSer)
                        .registerTypeAdapter(DateTime::class.java, dateTimeDeser)
                        .create()
        ))
    }.build().create(SiriusService::class.java) }
}


/** MICRO-SERVICES */
private fun Kodein.MainBuilder.setupMicroServices() {
    //Auth
    bind<InitToken>() with singleton { InitToken(instance()) }
    bind<RefreshTokenGuarded>() with singleton { RefreshTokenGuarded(instance()) }

    //KOS
    bind<LoadEvents>() with singleton { LoadEvents(instance(), instance(), instance()) }
    bind<LoadCourses>() with singleton { LoadCourses(instance(), instance(), instance()) }
    bind<LoadTeachers>() with singleton { LoadTeachers(instance(), instance(), instance(), instance()) }
    bind<LoadUser>() with singleton { LoadUser(instance(), instance()) }
    bind<LoadRooms>() with singleton { LoadRooms(instance(), instance()) }

    //Local
    bind<WipeData>() with singleton { WipeData(instance(), instance(), instance()) }

    //Other
    bind<FetchNews>() with singleton { FetchNews() }
}