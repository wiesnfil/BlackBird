package com.cvut.blackbird.flows

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.cvut.blackbird.R
import com.cvut.blackbird.model.services.UserInfo
import kotlinx.android.synthetic.main.black_bird_ac_activity.*

class BlackBirdMain : AppCompatActivity() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.black_bird_ac_activity)
        supportActionBar?.hide()
        navController = NavHostFragment.findNavController(navHost)

        if (UserInfo.username.isBlank())
            navController.navigate(R.id.toAuth)
    }

    override fun onSupportNavigateUp() = navController.navigateUp() //findNavController(navHost.id).navigateUp()
}