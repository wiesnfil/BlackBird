package com.cvut.blackbird.support.glue

import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class TextGlue(val sender: TextView) {
    var blankIgnore = false
        private set(value) {field = value}
    fun ignoreBlank(ignore: Boolean = true): TextGlue = this.apply { blankIgnore = ignore }
}
fun bindText(actor: TextView) = TextGlue(actor)


infix fun TextGlue.changeTo(receiver: (String) -> Unit) = this.apply {
    sender.onTextChanged { text ->
        if ((blankIgnore && text.isNotEmpty()) || !blankIgnore)
            receiver(text)
    }
}

infix fun TextGlue.changeTo(receiver: MutableLiveData<String>) = this.apply {
    sender.onTextChanged { text ->
        if ((blankIgnore && text.isNotEmpty()) || !blankIgnore)
            receiver.postValue(text)
    }
}

infix fun TextGlue.changeTo(receiver: TextView) = this.let {
    sender.onTextChanged { text ->
        if ((blankIgnore && text.isNotEmpty()) || !blankIgnore)
            receiver.text = text
    }
}

fun TextGlue.to(data: LiveData<String>, life: LifecycleOwner) = this.apply {
    life.observe(data) { text ->
        if (!(blankIgnore && text.isBlank()))
            sender.text = text
    }
}

fun TextView.onTextChanged(action: (text: String) -> Unit) {
    addTextChangedListener(object: TextWatcher {
        override fun afterTextChanged(s: Editable?) = Unit
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            action(s.toString())
        }

    })
}