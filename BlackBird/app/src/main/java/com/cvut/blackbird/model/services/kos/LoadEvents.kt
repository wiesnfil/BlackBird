package com.cvut.blackbird.model.services.kos

import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.database.EventDao
import com.cvut.blackbird.model.database.insertAll
import com.cvut.blackbird.model.services.*

class LoadEvents(
        override val authService: AuthService,
        private val siriusService: SiriusService,
        private val eventDao: EventDao
) : BBMicroService() {
    suspend operator fun invoke(all: Boolean = false): Result<Unit> {
        val result = fetch { siriusService.getEvents(UserInfo.username) }

        if (result is Success) {
            val events = result.value.events
            if (events != null)
                eventDao.insertAll(events)
            // TODO Should download only current semester and clean before inserting
            return Success(Unit)
        } else if (result is Failure)
            return Failure(result.message)

        return Failure("No error message")
    }
}