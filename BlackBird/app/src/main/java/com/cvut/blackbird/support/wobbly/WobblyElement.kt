package com.cvut.blackbird.support.wobbly

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce

abstract class WobblyElement: Button {

    companion object { const val TAG = "WElement" } //TAG to be suffixed with position

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    var elementDampingRatio = 0.65f
        set(value) {field = value; setupAnimation()}
    var elementStiffness = 1000f
        set(value) {field = value; setupAnimation()}

    var child: WobblyElement? = null
    var data: Any? = null

    private lateinit var animation: SpringAnimation

    fun initialize(data: Any?) {
//        isDuplicateParentStateEnabled = true
        isClickable = true
        this.data = data
        animation = SpringAnimation(this, DynamicAnimation.TRANSLATION_X)

        setupAnimation()
        setupLayout(data)
        setupChildAnim()
    }

    private fun setupAnimation() = animation.setSpring(
            SpringForce()
                    .setDampingRatio(elementDampingRatio)
                    .setStiffness(elementStiffness))

    private fun setupChildAnim() {
        animation.addUpdateListener { _, value, _ ->
            child?.moveTo(value)
        }
    }

    fun moveTo(position: Float) {
        if(data != null) //If element initialized
            animation.animateToFinalPosition(position)
    }

    fun getTail(): WobblyElement = child?.getTail() ?: this


//  ABSTRACT

    abstract fun setupLayout(data: Any?)
}