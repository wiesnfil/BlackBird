package com.cvut.blackbird.flows.tasks

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.chibatching.kotpref.livedata.asLiveData
import com.cvut.blackbird.extensions.*
import com.cvut.blackbird.flows.BlackBirdVM
import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.NotYet
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.database.EventDao
import com.cvut.blackbird.model.entities.DetailedEvent
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.entities.News
import com.cvut.blackbird.model.services.EventsMeta
import com.cvut.blackbird.model.services.kos.LoadEvents
import com.cvut.blackbird.model.services.other.FetchNews
import com.cvut.blackbird.support.glue.switchMap
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class TasksViewModel : BlackBirdVM() {

    private val evenDao by instance<EventDao>()

    private val fetchNewsMS by instance<FetchNews>()
    private val loadEventsMS by instance<LoadEvents>()

    private val _loadingStatus = MutableLiveData<Boolean>() withDefault false
    private val _refreshResult = MutableLiveData<Result<Unit>>() withDefault NotYet()
    private val _tasks = MutableLiveData<List<Task>>() withDefault listOf()
    private val _news = MutableLiveData<List<News>>() withDefault listOf()

    private val pinnedEvents =
        EventsMeta.asLiveData(EventsMeta::pinned)
                .switchMap { runBlocking(coroutineContext) {
                    evenDao.getDetailedEvents(it.map(String::toInt))
                } }

    val loadingStatus: LiveData<Boolean> get() = _loadingStatus
    val refreshResult: LiveData<Result<Unit>> get() = _refreshResult
    val tasks: LiveData<List<Task>> get() = _tasks

    init {
        fetchNews()
        configTasks()
    }

    private fun fetchNews() { startJob("fetchNews") {
        _refreshResult asProgressTo {
            val resultNews = fetchNewsMS()
            if (resultNews is Success) {
                _news.postValue(resultNews.value)
                loadEventsMS()
            } else Failure((resultNews as? Failure)
                    ?.message ?: "Unexpected error")
        }
    } indicateProgressBy _loadingStatus }

    private fun configTasks() { launch {
        val combined = combineLiveData(pinnedEvents, evenDao.futureExams(), _news) {
            pinned: List<DetailedEvent>?, exams: List<Event>?, news: List<News>? ->

            val resultTasks = ArrayList<Task>()
            if (pinned != null) resultTasks.addAll(pinned.map { PinnedTask(it) })
            if (exams != null) resultTasks.addAll(exams.map { ExamTask(it) })
            if (news != null && news.isNotEmpty()) resultTasks.add(NewsTask(news))

            resultTasks as List<Task>
        }

        withContext(Dispatchers.Main) { combined passTo _tasks}
    } }
}