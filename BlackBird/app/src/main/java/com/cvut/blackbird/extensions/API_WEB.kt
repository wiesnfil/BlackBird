package com.cvut.blackbird.extensions

import android.net.Uri

//"https://auth.fit.cvut.cz/oauth/authorize"

fun buildUri(baseUrl: String? = null, buildUrl: StringBuilder.() -> Unit): Uri {
    val builder = StringBuilder()
    if(baseUrl != null) builder.append(baseUrl)
    builder.buildUrl()
    return Uri.parse(builder.toString())
}
fun StringBuilder.appendParameters(paremeters: Map<String, String>) {
    append("?")
    for ((name, value) in paremeters) {
        if(last() != '?') append("&")
        append("$name=$value")
    }
}