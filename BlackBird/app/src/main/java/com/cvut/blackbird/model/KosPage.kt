package com.cvut.blackbird.model

import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Path
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml
abstract class KosPage<T: Any> {
        @PropertyElement(name = "atom:id")
        val path: String = ""

        @PropertyElement(name = "atom:updated")
        val updated: String = ""

        @Element(name = "atom:entry")
        val entries: List<T> = listOf()
}

@Xml
abstract class KosEntry<T: Any> {
        @PropertyElement(name = "atom:id")
        var id: String = ""

        @PropertyElement(name = "atom:updated")
        var path: String = ""

        @Path("atom:author")
        @PropertyElement(name = "atom:name")
        var author: String = ""

        @Element
        lateinit var content: T
}