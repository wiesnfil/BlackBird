package com.cvut.blackbird.support.wobbly

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.*
import com.github.florent37.kotlin.pleaseanimate.please

@SuppressLint("ViewConstructor")
class WobblyLineDescriptor(context: Context): TextView(context) {
    private lateinit var before: View
    private var after: View? = null
    private var space = 0f


    constructor(context: Context, before: View, after: View): this(context) {
        this.before = before
        this.after = after
    }

    constructor(context: Context, before: View, space: Float): this(context) {
        this.before = before
        this.space = space
    }

    init {
        tag = WobblyElement.TAG

        layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ).apply { setPadding(8.dpToPx, 4.dpToPx, 8.dpToPx, 4.dpToPx) }

        typeface = Typeface.SANS_SERIF
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)
        setTextColor(Color.WHITE)
        gravity = Gravity.CENTER

        setBackground()
    }

    private fun setBackground() {
        doOnLayout {
            if (it.height == 0) setBackground()
            else { it.background = GradientDrawable().apply {
                setColor(ContextCompat.getColor(context, R.color.backgroundDarkColor))
                shape = GradientDrawable.RECTANGLE
                cornerRadius = it.height.toFloat() / 2
                setStroke(2.dpToPx, Color.WHITE)
            } }
        }
    }

    public fun updatePosition() {
        globalX = ((after!!.globalCenterX + before.globalCenterX) / 2) - (width / 2)
        globalY = ((after!!.globalY + after!!.height + before.globalY) / 2) - (height / 2)
    }

    public fun withText(text: String) = this.apply { this.text = text }

    class Style(
            val background: Drawable?,
            val textColor: Int = Color.BLACK,
            val textSize: Float = 10f
    )
}