package com.cvut.blackbird.model.services.local

import com.cvut.blackbird.model.database.CourseDao
import com.cvut.blackbird.model.database.EventDao
import com.cvut.blackbird.model.database.TeacherDao
import com.cvut.blackbird.model.services.*

class WipeData(
        private val eventDao: EventDao,
        private val courseDao: CourseDao,
        private val teacherDao: TeacherDao
) {

    suspend operator fun invoke() {

        //DB
        eventDao.deleteAllEvents()
        eventDao.deleteAllNotes()
        courseDao.deleteAll()
        teacherDao.deleteAll()

        //SharedPrefs
        UserInfo.clear()
        AuthInfo.clear()
        EventsMeta.clear()
    }
}