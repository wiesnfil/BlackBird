package com.cvut.blackbird.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.cvut.blackbird.model.entities.Course
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.entities.EventNote
import com.cvut.blackbird.model.entities.Teacher
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat


@Database(entities = [Event::class, EventNote::class, Course::class, Teacher::class], version = 10)
@TypeConverters(DBTypeConverters::class)
abstract class BlackBirdDB: RoomDatabase() {
    abstract fun eventDao(): EventDao
    abstract fun courseDao(): CourseDao
    abstract fun teacherDao(): TeacherDao

}

class DBTypeConverters {

    @TypeConverter
    fun toDate(value: Long?): DateTime? = if (value == null) null else DateTime(value)

    @TypeConverter
    fun toLong(value: DateTime?): Long? = value?.millis

    @TypeConverter
    fun restoreList(listOfString: String): List<String> =
            if (listOfString != "null") Gson().fromJson(listOfString, object : TypeToken<List<String>>(){}.type)
            else listOf()

    @TypeConverter
    fun saveList(listOfString: List<String>?) = Gson().toJson(listOfString)
}