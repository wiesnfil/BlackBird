package com.cvut.blackbird.model.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.tickaroo.tikxml.annotation.Path
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml
@Entity
data class Teacher(
        @Path("atom:content")
        @PropertyElement val firstName: String,

        @Path("atom:content")
        @PropertyElement val lastName: String,

        @PrimaryKey
        @Path("atom:content")
        @PropertyElement val personalNumber: Int,

        @Path("atom:content")
        @PropertyElement val titlesPost: String?,

        @Path("atom:content")
        @PropertyElement val titlesPre: String?,

        @Path("atom:content")
        @PropertyElement val username: String?,

        @Path("atom:content")
        @PropertyElement val division: String,

        @Path("atom:content")
        @PropertyElement val email: String,

        @Path("atom:content")
        @PropertyElement val extern: Boolean,

        @Path("atom:content")
        @PropertyElement val phone: String?
)