package com.cvut.blackbird.support.dynamicViewPager

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.viewpager.widget.ViewPager


class MeasuringViewPager : ViewPager {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val v = findViewWithTag<View>(currentItem)
        v.measure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))

        super.onMeasure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(v.measuredHeight, View.MeasureSpec.EXACTLY))
    }
}