package com.cvut.blackbird.flows.tasks

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.*
import com.cvut.blackbird.BlackBirdAC

import com.cvut.blackbird.R
import com.cvut.blackbird.flows.detail.EventDetailViewModel
import com.cvut.blackbird.flows.findMainNav
import com.cvut.blackbird.flows.timetable.TimetableViewModel
import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.entities.News
import com.cvut.blackbird.support.glue.bind
import com.cvut.blackbird.support.glue.observe
import com.cvut.blackbird.support.glue.toPassValueTo
import kotlinx.android.synthetic.main.tasks_fragment.*
import kotlinx.coroutines.launch

class TasksFragment : Fragment() {
    private lateinit var viewModel: TasksViewModel
    private lateinit var eventViewModel: EventDetailViewModel
    private lateinit var taskAdapter: TasksListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.tasks_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(TasksViewModel::class.java)

        setupUi()
        setupBinding()

        eventViewModel = ViewModelProviders.of(activity!!).get(EventDetailViewModel::class.java)

        // Preload timetable data
        ViewModelProviders.of(activity!!).get(TimetableViewModel::class.java)
    }

    private fun setupUi() {
        taskAdapter = TasksListAdapter {
            when (it) {
                is PinnedTask -> goToEventDetail(it.event.event)
                is ExamTask -> goToEventDetail(it.event)
                is NewsTask -> goToNews(it.lastClickedStory)
            }
        }
        examsRecyclerView.adapter = taskAdapter
        examsRecyclerView.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
    }

    private fun setupBinding() {
        bind(viewModel.tasks) toPassValueTo taskAdapter::submitList
        observe(viewModel.refreshResult) {
            Log.d(BlackBirdAC.LOG_TAG, (it as? Failure)?.message ?: it.toString())
        }
    }

    private fun goToEventDetail(event: Event) {
        eventViewModel.setEvent(event)
        findMainNav()?.navigate(R.id.toEventDetail)
    }

    private fun goToNews(news: News?) {
        if (news == null) return
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(news.link))
        if (activity != null && intent.resolveActivity(activity!!.packageManager) != null) {
            startActivity(intent)
        }
    }
}
