package com.cvut.blackbird.model.entities

import androidx.room.*

@Entity(indices = [Index(value = ["eventId"], unique = true)])
data class EventNote(
        @PrimaryKey(autoGenerate = true)
        val noteId: Int,

        @ForeignKey(
                entity = Event::class,
                parentColumns = ["id"],
                childColumns = ["eventId"],
                onUpdate = ForeignKey.CASCADE)
        val eventId: Int,

        var note: String
) { @Ignore constructor(eventId: Int) : this(0, eventId, "") }