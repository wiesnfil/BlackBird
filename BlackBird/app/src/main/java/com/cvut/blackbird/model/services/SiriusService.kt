package com.cvut.blackbird.model.services

import com.cvut.blackbird.model.entities.EventParent
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface SiriusService {
    companion object {
        const val url = "https://sirius.fit.cvut.cz/api/v1/"
    }

    @GET("people/{username}/events")
    fun getEvents(
            @Path("username") username: String = UserInfo.username,
            @Query("limit") resultsLimit: Int = 1000,
            @Header("Authorization") token: String = AuthInfo.accessTokenHeader
    ): Call<EventParent>

//    @FormUrlEncoded
//    @GET("exams")
//    fun getExams(
//            @Header("lang") lang: String = "en",
//            @Field("limit") resultsLimit: Int = 1000
//    ): Observable<Response<ExamTask>>
//
//    @FormUrlEncoded
//    @GET("exams/{id}")
//    fun getExamById(
//            @Path("id") examId: Int
//    ): Observable<Response<ExamTask>>
}