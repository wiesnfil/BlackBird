package com.cvut.blackbird.model.services

import com.cvut.blackbird.model.KosEntry
import com.cvut.blackbird.model.KosPage
import com.cvut.blackbird.model.entities.*
import retrofit2.Call
import retrofit2.http.*

class KosParameters(
        val semester: String = "curr",
        val offset: Int = 0,
        val limit: Int = 1000,
        val lang: String = "en",
        val multilang: Boolean = false
)

interface KosService {
    companion object {
        const val url = "https://kosapi.feld.cvut.cz/api/3/"
    }



    /**
     * Student
     */

    @GET("students/{user}")
    fun getStudent(
            @Path("user") user: String,
            @Header("Authorization") token: String = AuthInfo.accessTokenHeader
    ): Call<Student>

    @GET("students/{user}/enrolledCourses")
    fun getStudentsCourses(
            @Path("user") user: String = UserInfo.username,
            @Query("sem") semester: String = "none",
            @Header("Authorization") token: String = AuthInfo.accessTokenHeader,
            @Query("limit") resultsLimit: Int = 1000,
            @Query("lang") lang: String = "en",
            @Query("multilang") multilang: Boolean = false
    ): Call<EnrolledCourseRoot>



    /**
     * Course
     */

    @GET("courses/{course}")
    fun getCourse(
            @Path("course") course: String,
            @Header("Authorization") token: String = AuthInfo.accessTokenHeader,
            @Query("lang") lang: String = "en",
            @Query("multilang") multilang: Boolean = false
    ): Call<Course>



    /**
     * Teacher
     */

    @GET("teachers/{teacher}")
    fun getTeacher(
            @Path("teacher") teacherUsername: String,
            @Header("Authorization") token: String = AuthInfo.accessTokenHeader,
            @Query("lang") lang: String = "en",
            @Query("multilang") multilang: Boolean = false
    ): Call<Teacher>

    /**
     *  Room
     */

    @GET("rooms/{code}")
    fun getRooms(
            @Path("code") roomCode: String,

            @Header("Authorization") token: String = AuthInfo.accessTokenHeader,

            @Query("offset") resultOffset: Int = 0,
            @Query("limit") resultsLimit: Int = 1000,
            @Query("lang") lang: String = "en",
            @Query("multilang") multilang: Boolean = false
    ): Call<KosPage<KosEntry<Any>>>

}

