package com.cvut.blackbird.model.services

import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.bulk
import com.chibatching.kotpref.livedata.asLiveData
import com.cvut.blackbird.model.entities.Student

object AuthInfo: KotprefModel() {
    var accessToken by stringPref()
    var refreshToken by stringPref()

    val accessTokenHeader get() = "Bearer $accessToken"
}

object UserInfo: KotprefModel() {
    var username by stringPref()
    var facultyAbbr by stringPref()

    var firstName by stringPref()
    var surname by stringPref()
    var email by stringPref()
    var faculty by stringPref()
    var programme by stringPref()
    var studyGroup by intPref()

    fun setStudent(student: Student): Unit = bulk {
        username = student.username ?: ""
        firstName = student.firstName ?: ""
        surname = student.surname ?: ""
        email = student.email ?: ""
        faculty = student.faculty ?: ""
        programme = student.programme ?: ""
        studyGroup = student.studyGroup ?: -1
    }
}

object EventsMeta: KotprefModel() {
    val pinned by stringSetPref(commitByDefault = true)

    fun togglePinned(event: String) {
        if(pinned.contains(event)) pinned.remove(event)
        else pinned.add(event)
    }
}