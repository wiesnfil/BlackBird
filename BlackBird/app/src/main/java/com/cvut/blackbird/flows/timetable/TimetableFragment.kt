package com.cvut.blackbird.flows.timetable

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.dpToPx
import com.cvut.blackbird.flows.detail.EventDetailViewModel
import com.cvut.blackbird.flows.findMainNav
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.support.glue.bind
import com.cvut.blackbird.support.glue.clickTo
import com.cvut.blackbird.support.glue.toPassValueTo
import com.cvut.blackbird.support.glue.withMap
import com.cvut.blackbird.support.wobbly.WobblyAdapter
import com.cvut.blackbird.support.wobbly.WobblyElement
import kotlinx.android.synthetic.main.timetable_fragment.*


class TimetableFragment : Fragment() {

    companion object {
        const val MIN_DIFF_TO_NOTIFY = 15
    }

    private var first: Boolean = true
    private lateinit var viewModel: TimetableViewModel
    private lateinit var eventViewModel: EventDetailViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.timetable_fragment, container, false)


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(TimetableViewModel::class.java)
        eventViewModel = ViewModelProviders.of(activity!!).get(EventDetailViewModel::class.java)

        wobblyTimetable.adapter = getAdapter()
        setupBinding()
    }

    override fun onPause() {
        super.onPause()
        viewModel.scrollState.value = Pair(
                wobblyTimetable.wobblyScrollX,
                wobblyTimetable.wobblyScrollY
        )
    }

    private fun setupBinding() {
        bind(dotsLeftArrowBtn) clickTo viewModel::previousWeek
        bind(dotsRightArrowBtn) clickTo viewModel::nextWeek

        bind(viewModel.timetable) toPassValueTo ::onWeekChange
        bind(viewModel.scrollState) toPassValueTo wobblyTimetable::setScrollInitialState

        bind(viewModel.displayedWeek) withMap { it.monthOfYear().asText +
                " ${it.withDayOfWeek(1).dayOfMonth} - ${it.withDayOfWeek(5).dayOfMonth}"
        } toPassValueTo dotsWeekDateTxt::setText
    }

    private fun onWeekChange(events: Map<Int,List<Event>>) {
        if (!first)
            wobblyTimetable.fadeRefresh { setupLineDescriptors(events) }
        else setupLineDescriptors(events)
        first = false
    }
    private fun setupLineDescriptors(events: Map<Int,List<Event>>) {
        events.forEach { (day, events) ->
            wobblyTimetable.addLineDescription(day - 1, 0, events.first().startsAt.toString("HH:mm"))
            events.forEachIndexed { index, event ->
                if (index > 0) {
                    val difference = event.startsAt.minuteOfDay - events[index - 1].endsAt.minuteOfDay
                    if (difference > MIN_DIFF_TO_NOTIFY)
                        wobblyTimetable.addLineDescription(day - 1, index, buildString {
                            if (difference < 60) append("$difference minutes")
                            else {
                                if (difference / 60 > 0) append((difference / 60).toString() + "h ")
                                if (difference % 60 > 0) append((difference % 60).toString() + "min")
                                else {
                                    delete(length - 2, length)
                                    append(" hours")
                                }
                            }
                        })
                }

            }
        }
    }

    private fun onLectureClick(element: View) {
        if (element is TimetableBall) {
            eventViewModel.setEvent(element.payload)
            findMainNav()!!.navigate(R.id.toEventDetail)
        }
    }

    private val days = arrayOf("Mon", "Tue", "Wed", "Thu", "Fri")
    fun getAdapter() = object : WobblyAdapter<Event> {
        override fun getColumnCount() = days.size

        override fun getItem(position: Int, column: Int) = viewModel
                .timetable.value?.
                get(column+1)?.
                get(position)?: Event.empty

        override fun getItemView(position: Int, column: Int): WobblyElement = TimetableBall(activity!!)
                .apply { setOnClickListener { onLectureClick(it) } }

        override fun getItemCount(column: Int): Int = viewModel.timetable
                .value?.get(column+1)?.size ?: 0

        override fun getHeader(column: Int) = HeaderBall(activity!!, days[column])

        override fun spacingBefore(position: Int, column: Int): Float {
            return if (position > 0) {
                var difference = getItem(position, column).startsAt.minuteOfDay -
                        getItem(position - 1, column).endsAt.minuteOfDay
                difference = difference.coerceAtLeast(10)

                if (difference in (MIN_DIFF_TO_NOTIFY + 1)..59)
                    difference = 60

                difference.toFloat()
                        .dpToPx / 2

            } else 30f.dpToPx
        }
    }

    class HeaderBall(context: Context, val text:String): TextView(context) {
        init {
            background = GradientDrawable().apply {
                setColor(Color.DKGRAY)
                shape = GradientDrawable.OVAL }
            layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            ).apply { setMargins(10.dpToPx,0,10.dpToPx,0) }

            setText(text)

            typeface = Typeface.SANS_SERIF
            setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            setTextColor(Color.WHITE)
            gravity = Gravity.CENTER
            elevation = 2f
            width = 75.dpToPx
            height = 75.dpToPx
        }
    }
}