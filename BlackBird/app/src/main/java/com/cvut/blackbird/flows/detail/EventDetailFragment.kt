package com.cvut.blackbird.flows.detail


import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.courseAbbr
import com.cvut.blackbird.extensions.getColor
import com.cvut.blackbird.model.entities.*
import com.cvut.blackbird.support.glue.*
import kotlinx.android.synthetic.main.event_detail_fragment.*


class EventDetailFragment : Fragment() {
    private lateinit var detailedEvent: DetailedEvent
    private lateinit var viewModel: EventDetailViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(EventDetailViewModel::class.java)
        setupBinding()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.event_detail_fragment, container, false)

    private fun setupBinding() {
        bind(viewModel.detailedEvent) toPassValueTo ::setupEvent
        bind(detail_pin) clickToInvoke ::onPinnedStateChange
        //        bindText(detail_notePersonalContent) changeTo viewModel::changeEventNote
    }

    private fun setupEvent(detEvent: DetailedEvent) {
        this.detailedEvent = detEvent

        detail_abbr.background = when(detEvent.event.eventType) {
            EventType.LECTURE  -> context?.resources?.getDrawable(
                    R.drawable.ic_timetable_lecture, context?.theme)
            EventType.TUTORIAL -> context?.resources?.getDrawable(
                    R.drawable.ic_timetable_tutorial, context?.theme)
            else -> context?.resources?.getDrawable(
                    R.drawable.ic_timetable_info, context?.theme)
        }

        detail_abbr.text = detEvent.courseEnt?.code?.courseAbbr ?: "-"
        detail_codeName.text = detEvent.courseEnt?.code
        detail_sequenceNum.text = detEvent.event.sequenceNum.toString()
        detail_lengthTxt.text = "${detEvent.event.endsAt.minuteOfDay - detEvent.event.startsAt.minuteOfDay} min"
        detail_capacityTxt.text = "${detEvent.event.occupied}/${detEvent.event.capacity}"
        detail_timeTxt.text = "${detEvent.event.startsAt.toString("HH:mm")} - ${detEvent.event.endsAt.toString("HH:mm")}"
        detail_teachersContent.text = detEvent.event.linked?.teachers?.firstOrNull() ?: "None"
        detail_parallelContent.text = if (detEvent.event.parallel.isNotBlank()) detEvent.event.parallel else "-"
        detail_roomContent.text = detEvent.event.linked?.room ?: "None"
        detail_noteContent.text = detEvent.event.note?.cs ?: "None"
        detail_notePersonalContent.setText(detEvent.userNotes.firstOrNull()?.note ?: "")

        setupCourse(detEvent.courseEnt)
        setupTeachers(detEvent.teacherList)

        refreshPinTint()
    }

    private fun setupCourse(course: Course?) {
        if (course != null)
            detail_fullName.text = course.name
    }

    private fun setupTeachers(teachers: List<Teacher>) {
        if (teachers.isEmpty()) return

        detail_teachersContent.text = ""
        val teachersText = buildString {
            for (teacher in teachers)
                append("${teacher.firstName} ${teacher.lastName}\n")
        }
        detail_teachersContent.text = teachersText.dropLast(1)
    }

    private fun onPinnedStateChange() = detailedEvent.event
            .apply {
                isPinned = !isPinned
                refreshPinTint()
            }

    private fun refreshPinTint() = detail_pin.let {
        it.imageTintList =
                if (detailedEvent.event.isPinned) ColorStateList.valueOf(getColor(R.color.colorTextLight))
                else ColorStateList.valueOf(getColor(R.color.colorTextDark))
    }


    override fun onResume() {
        super.onResume()
//        if (viewModel.eventInitialized && viewModel.detailedEvent.value?.event == Event.empty)
//            findNavController().navigateUp()
    }

    override fun onPause() {
        super.onPause()
        viewModel.changeEventNote(detail_notePersonalContent.text.toString())
    }
}