package com.cvut.blackbird.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cvut.blackbird.model.entities.Teacher

@Dao
interface TeacherDao {
    @Query("SELECT * FROM Teacher WHERE username = :username")
    fun getTeacher(username: String): Teacher?

    @Query("SELECT * FROM Teacher WHERE username in (:usernames)")
    fun getTeachers(usernames: List<String>): List<Teacher>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(courses: List<Teacher>)

    @Query("DELETE FROM Teacher")
    fun deleteAll()
}