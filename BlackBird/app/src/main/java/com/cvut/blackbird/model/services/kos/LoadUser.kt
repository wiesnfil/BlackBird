package com.cvut.blackbird.model.services.kos

import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.services.AuthService
import com.cvut.blackbird.model.services.BBMicroService
import com.cvut.blackbird.model.services.KosService
import com.cvut.blackbird.model.services.UserInfo

class LoadUser(
        override val authService: AuthService,
        private val kosService: KosService
) : BBMicroService() {

    suspend operator fun invoke(): Result<Unit> {
        val userInfo = fetch { authService.getUserInfo() }

        val user = if (userInfo is Success) {
            UserInfo.facultyAbbr = userInfo.value.email
                    .split('.','@')[1]
            fetch { kosService.getStudent(userInfo.value.username) }
        } else Failure((userInfo as? Failure)?.message
                ?: "Unexpected Error")

        return if (user is Success) {
            UserInfo.setStudent(user.value)
            Success(Unit)
        } else Failure((user as? Failure)?.message
                ?: "Unexpected Error")
    }
}