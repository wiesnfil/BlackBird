package com.cvut.blackbird.model.services

import com.cvut.blackbird.extensions.base64Encoded
import com.cvut.blackbird.model.entities.AccessToken
import com.cvut.blackbird.model.entities.User
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface AuthService {
    companion object Client {
        const val url = "https://auth.fit.cvut.cz/oauth/"
        const val clientId = "7d7f8a83-b883-4029-b747-d3a99a9da235"
        const val clientSecret = "nCx6ggFvQLpfe4zTX61LYrCJ0Eygfa49"

        val auth:String get() = "Basic " + ("$clientId:$clientSecret").base64Encoded()
    }

    /**
     * @param clientAuth String made of client id and client secret encoded in base64
     * string should be "Basic [clientId]:[clientSecret]" and encoded
     */
    
    @FormUrlEncoded
    @POST("token")
    fun getToken(
            @Field("code") code: String,
            @Header("Authorization") clientAuth: String = AuthService.auth,
            @Field("redirect_uri") redirectUri: String = "kosapp://callback",
            @Field("grant_type") grantType: String = "authorization_code"
    ): Call<AccessToken>

    @FormUrlEncoded
    @POST("token")
    fun refreshToken(
            @Field("refresh_token") refreshToken: String = AuthInfo.refreshToken,
            @Header("Authorization") clientAuth: String = AuthService.auth,
            @Field("grant_type") grantType: String = "refresh_token"
    ): Call<AccessToken>

    @GET("userinfo")
    fun getUserInfo(
            @Header("Authorization") token: String = AuthInfo.accessTokenHeader
    ): Call<User>
}