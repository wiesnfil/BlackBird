package com.cvut.blackbird.widget

import android.app.Service
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.IBinder

class NextEventService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        sendBroadcast(Intent(this, NextEventWidget::class.java).apply {
            action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            putExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_IDS,
                    AppWidgetManager.getInstance(this@NextEventService)
                            .getAppWidgetIds(ComponentName(this@NextEventService, NextEventWidget::class.java))
            )
        })

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent): IBinder? = null
}
