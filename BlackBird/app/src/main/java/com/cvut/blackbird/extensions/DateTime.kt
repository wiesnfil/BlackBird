package com.cvut.blackbird.extensions

import org.joda.time.DateTime

fun DateTime.atEndOfTheDay(): DateTime {
    return this.withHourOfDay(23)
            .withMinuteOfHour(59)
            .withSecondOfMinute(59)
            .withMillisOfSecond(99)
}

fun DateTime.atStartOfTheDay(): DateTime {
    return this.withHourOfDay(0)
            .withMinuteOfHour(0)
            .withSecondOfMinute(0)
            .withMillisOfSecond(0)
}