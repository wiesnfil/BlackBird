package com.cvut.blackbird.model.support

sealed class Result<T>
class Success<T>(val value: T): Result<T>()
class Failure<T>(val message: String): Result<T>()
class Loading<T>: Result<T>()
class NotYet<T>: Result<T>()

inline fun <T, R> Result<T>.map(f: (T)->R): Result<R> =
        when(this) {
            is Success -> Success(f(this.value))
            is Failure -> Failure(this.message)
            is Loading -> Loading()
            is NotYet -> NotYet()
        }