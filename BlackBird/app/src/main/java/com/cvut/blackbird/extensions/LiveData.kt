@file:Suppress("unused")

package com.cvut.blackbird.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.MutableLiveData
import com.cvut.blackbird.model.support.Loading
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.support.glue.Glue
import com.cvut.blackbird.support.glue.SuperGlue
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


infix fun<T> Glue.enabledToSuccessOf(source: LiveData<Result<T>>) {
    source.observe(life, Observer { actor.isEnabled = it is Success })
}

infix fun<T> SuperGlue<Result<T>>.toNotifyOnSuccess(action: () -> Unit) = this.apply {
    emmiter.observe(life, Observer { if (it is Success) action() })
}

infix fun<T> MutableLiveData<T>.withDefault(init: T) = apply { value = init }

fun <T> LiveData<T>.haveValue() = value != null

fun <T> LiveData<T>.hasNoValue(): Boolean {
    return value == null
}


inline fun MutableLiveData<Boolean>.asBoolProgressStatus(job: () -> Unit) {
    postValue(true)
    job.invoke()
    postValue(false)
}

infix fun Job.indicateProgressBy(status: MutableLiveData<Boolean>): Job {
    status.postValue(true)
    invokeOnCompletion { status.postValue(false) }
    return this
}


inline infix fun<T> MutableLiveData<Result<T>>.asProgressStatus(job: () -> Result<T>) {
    postValue(Loading())
    postValue(job.invoke())
}
inline infix fun<T> MutableLiveData<Result<T>>.asProgressTo(job: () -> Result<T>) {
    postValue(Loading())
    postValue(job.invoke())
}
inline infix fun<T> MutableLiveData<Result<T>>.fetchUsingJob(crossinline job: suspend () -> Result<T>) =
        GlobalScope.launch {
            postValue(Loading())
            postValue(job.invoke())
        }

infix fun <T> LiveData<T>.passTo(data: MutableLiveData<T>) = observeForever { data.postValue(it) }


/**
 * LiveData combining
 */

fun <A, B, R> LiveData<A>.combineWith(data: LiveData<B>, combine: (data1: A?, data2: B?) -> R) =
        combineLiveData(this, data, combine)

fun <A, B, R> combineLiveData(
        source1: LiveData<A>,
        source2: LiveData<B>,
        combine: (data1: A?, data2: B?) -> R)
        : LiveData<R> {

    return MediatorLiveData<R>().apply {
        var data1: A? = null
        var data2: B? = null

        addSource(source1) {
            data1 = it
            value = combine(data1, data2)
        }

        addSource(source2) {
            data2 = it
            value = combine(data1, data2)
        }
    }
}

fun <A, B, C, R> combineLiveData(
        source1: LiveData<A>,
        source2: LiveData<B>,
        source3: LiveData<C>,
        combine: (data1: A?, data2: B?, data3: C?) -> R)
        : LiveData<R> {

    return MediatorLiveData<R>().apply {
        var data1: A? = null
        var data2: B? = null
        var data3: C? = null

        addSource(source1) {
            data1 = it
            value = combine(data1, data2, data3)
        }

        addSource(source2) {
            data2 = it
            value = combine(data1, data2, data3)
        }

        addSource(source3) {
            data3 = it
            value = combine(data1, data2, data3)
        }
    }
}

fun <A, B, C, D, R> combineLiveData(
        source1: LiveData<A>,
        source2: LiveData<B>,
        source3: LiveData<C>,
        source4: LiveData<D>,
        combine: (data1: A?, data2: B?, data3: C?, data4: D?) -> R)
        : LiveData<R> {

    return MediatorLiveData<R>().apply {
        var data1: A? = null
        var data2: B? = null
        var data3: C? = null
        var data4: D? = null

        addSource(source1) {
            data1 = it
            value = combine(data1, data2, data3, data4)
        }

        addSource(source2) {
            data2 = it
            value = combine(data1, data2, data3, data4)
        }

        addSource(source3) {
            data3 = it
            value = combine(data1, data2, data3, data4)
        }

        addSource(source4) {
            data4 = it
            value = combine(data1, data2, data3, data4)
        }
    }
}

/**
 *  JOBS
 */

enum class JobStrategy { CANCEL, EXIT }
inline fun doWith(job: Job?, strategy: JobStrategy = JobStrategy.CANCEL, crossinline todo: () -> Job): Job {
    if (job?.isActive == true) {
        when (strategy) {
            JobStrategy.CANCEL -> job.cancel()
            JobStrategy.EXIT -> return job
        }
    }
    return todo()
}