package com.cvut.blackbird.model.support

import com.google.gson.JsonParser

enum class AuthResult(val critical: Boolean){
    SUCCESS(false),
    NOT_YET(false),
    INVALID_REQUEST(true),
    INVALID_CLIENT(true),
    INVALID_GRANT(false),
    UNAUTHORIZED_CLIENT(true),
    UNSUPPORTED_GRANT_TYPE(true),
    INVALID_SCOPE(true),
    UNEXPECTED_ERROR(true);

    var errorDesc: String? = null
    var errorUri: String? = null

    fun setDesc(message: String): AuthResult {
        errorDesc = message
        return this
    }

    companion object {
        fun resolveReturnJson(json: String?, errorMessage:String = "Unidentified error"): AuthResult {
            val jsonObj = JsonParser().parse(json).asJsonObject
            val result = AuthResult.valueOf(jsonObj.get("error")?.asString?.toUpperCase() ?: AuthResult.UNEXPECTED_ERROR.name)
            if(jsonObj.has("error_description"))
                result.errorDesc = jsonObj.get("error_description").asString
            if(jsonObj.has("error_uri"))
                result.errorUri = jsonObj.get("error_uri").asString
            if (result.errorDesc == null) result.errorDesc = errorMessage
            return result
        }
    }
}