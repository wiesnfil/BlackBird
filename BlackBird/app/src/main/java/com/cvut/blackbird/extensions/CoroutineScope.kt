package com.cvut.blackbird.extensions

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.withContext

suspend fun <T> CoroutineScope.onMainThread(
        job: suspend CoroutineScope.() -> T
) = withContext(Dispatchers.Main, job)