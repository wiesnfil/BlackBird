package com.cvut.blackbird.flows.tasks

import android.animation.LayoutTransition
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.cvut.blackbird.BlackBirdAC
import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.*
import com.cvut.blackbird.model.entities.DetailedEvent
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.entities.EventType
import com.cvut.blackbird.model.entities.News
import com.cvut.blackbird.support.kolor.Kolor
import kotlinx.android.synthetic.main.exam_list_row.view.*
import kotlinx.android.synthetic.main.tasks_news_layout.view.*

import kotlinx.android.synthetic.main.tasks_pinned.view.*
import org.joda.time.DateTime
import org.joda.time.Days
import org.joda.time.Minutes


sealed class Task(val id: Int)
data class ExamTask(val event: Event): Task(event.id)
data class PinnedTask(val event: DetailedEvent): Task(event.event.id)
data class NewsTask(val news: List<News>): Task(news.hashCode()) {
    public var lastClickedStory: News? = null
}

class TasksListAdapter(private val clickListener: (Task) -> Unit): ListAdapter<Task,  TaskViewHolder>(
        object : DiffUtil.ItemCallback<Task>() {
            override fun areItemsTheSame(oldItem: Task, newItem: Task) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Task, newItem: Task) = oldItem == newItem
        }
) {
    companion object {
        const val EXAM = 1
        const val PINNED = 2
        const val NEWS = 3
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is ExamTask -> EXAM
            is PinnedTask -> PINNED
            is NewsTask -> NEWS
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  TaskViewHolder {
        return when (viewType) {
            EXAM -> ExamViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.exam_list_row, parent, false), parent.context)
            PINNED -> PinnedViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.tasks_pinned, parent, false), parent.context)
            NEWS -> NewsViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.tasks_news_layout, parent, false), parent.context)
            else -> throw Exception("This view type is not implemented")
        }
    }

    override fun onBindViewHolder(holder:  TaskViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
        setAnimation(holder, position)
    }

    override fun onViewDetachedFromWindow(holder: TaskViewHolder) {
        holder.clearAnimation()
        super.onViewDetachedFromWindow(holder)
    }


    private var lastPosition = -1
    private fun setAnimation(holder: TaskViewHolder, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(holder.context, android.R.anim.fade_in)
            holder.view.startAnimation(animation)
            lastPosition = position
        }
    }
}

sealed class TaskViewHolder(val view: View, val context: Context) : RecyclerView.ViewHolder(view) {
    abstract fun bind(task: Task, clickListener: (Task) -> Unit)
    fun clearAnimation() = view.clearAnimation()
}

class ExamViewHolder(view: View, context: Context) : TaskViewHolder(view, context) {
    override fun bind(task: Task, clickListener: (Task) -> Unit) {
        val event = (task as? ExamTask)?.event ?: throw TypeCastException("You have to pass ExamTask to ExamViewHolder dumbass")

        view.examLengthTxt.text = "${Minutes.minutesBetween(event.startsAt, event.endsAt).minutes} minutes"
        view.examOccupiedTxt.text = event.occupied.toString()
        view.examCapacityTxt.text = event.capacity.toString()
        view.examDueTimeTxt.text = "in ${Days.daysBetween(DateTime.now(),event.startsAt).days} days"
        view.examRoomTxt.text = event.linked?.room

        if (event.note != null)
            view.examInfoTxt.text = buildString {
                append(event.note.cs)
                if (event.note.en != null) append(event.note.en)
                view.examInfoTxt.visibility = View.VISIBLE
            }


        if(event.linked?.course != null && event.linked.course.length >= 3) {
            view.examCourseAbbr.text = event.linked.course.courseAbbr
            view.examCourseAbbr.background = when(event.eventType) {
                EventType.LECTURE  ->
                    context.resources.getDrawable(R.drawable.ic_timetable_lecture, context.theme)
                EventType.TUTORIAL ->
                    context.resources.getDrawable(R.drawable.ic_timetable_tutorial, context.theme)
                else                                        ->
                    context.resources.getDrawable(R.drawable.ic_timetable_info, context.theme)
            }
        }

        view.setOnClickListener{clickListener(task)}
    }
}

class PinnedViewHolder(view: View, context: Context) : TaskViewHolder(view, context) {
    override fun bind(task: Task, clickListener: (Task) -> Unit) {
        val pinnedEvent = (task as? PinnedTask)?.event ?: throw TypeCastException("You have to pass pinnedTask to PinnedViewHolder dumbass")

        view.pin_eventAbbr.background = when(pinnedEvent.event.eventType) {
            EventType.LECTURE  ->
                context.resources.getDrawable(R.drawable.ic_timetable_lecture, context.theme)
            EventType.TUTORIAL ->
                context.resources.getDrawable(R.drawable.ic_timetable_tutorial, context.theme)
            else                                        ->
                context.resources.getDrawable(R.drawable.ic_timetable_info, context.theme)
        }

        view.pin_eventAbbr.text = pinnedEvent.event.linked?.course?.courseAbbr ?: "-"
        view.pin_timeLeftTxt.text = "Due in ${Days.daysBetween(DateTime.now(),pinnedEvent.event.startsAt).days} days"

        if (pinnedEvent.event.note?.cs != null || pinnedEvent.userNote.note.isNotBlank())
            view.pin_description.setVisible()
        pinnedEvent.event.note?.let { view.pin_description.text = it.cs }
        view.pin_description.let {
            it.text = it.text.toString() +
                    if (pinnedEvent.userNote.note.isNotBlank())
                        pinnedEvent.userNote.note
                    else ""
        }


        view.pin_unpinBtn.setOnClickListener { pinnedEvent.event.isPinned = !pinnedEvent.event.isPinned }

        view.setOnClickListener{clickListener(task)}
    }
}

class NewsViewHolder(view: View, context: Context) : TaskViewHolder(view, context) {
    override fun bind(task: Task, clickListener: (Task) -> Unit) {
        val newsTask = (task as? NewsTask) ?: throw TypeCastException("You have to pass NewsTask to NewsViewHolder dumbass")
        val news = newsTask.news.asReversed()
        val pager = view.news_pager
        (view as CardView).layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
        view.news_dotTabs.setupWithViewPager(pager)
        pager.adapter = object : PagerAdapter() {
            override fun instantiateItem(container: ViewGroup, position: Int): Any {
                val article = TextView(context).apply {
                    setHtmlText(news[position].title)
                    setupArticleLayout()
                    tag = position

                    setOnClickListener { clickListener(task) }
                }
                container.addView(article)
                return article
            }

            override fun destroyItem(container: ViewGroup, position: Int, item: Any) =
                    container.removeView(item as View)
            override fun getCount() = news.size
            override fun isViewFromObject(view: View, item: Any) = view == item
        }

        pager.addOnSelectedListener { position ->
            newsTask.lastClickedStory = news[position]
            pager.requestLayout()
        }

        (itemView.layoutParams as StaggeredGridLayoutManager.LayoutParams)
                .isFullSpan = true
    }

    @Suppress("DEPRECATION")
    fun TextView.setHtmlText(text: String) = setText(
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
            } else Html.fromHtml(text)
    )

    fun TextView.setupArticleLayout() {
        layoutParams = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
            setMargins(0,8.dpToPx, 0,0)
        }
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        setTextColor(Color.WHITE)
    }

    fun ViewPager.addOnSelectedListener(listener: (position: Int) -> Unit) {
        addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) = Unit
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) = Unit
            override fun onPageSelected(position: Int) { listener(position) }
        })
    }

}