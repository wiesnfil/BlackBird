package com.cvut.blackbird.flows

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cvut.blackbird.BlackBirdAC
import com.cvut.blackbird.extensions.JobStrategy
import com.cvut.blackbird.extensions.doWith
import com.cvut.blackbird.extensions.withDefault
import kotlinx.coroutines.*
import kotlinx.coroutines.android.Main
import org.kodein.di.KodeinAware
import kotlin.coroutines.CoroutineContext

abstract class BlackBirdVM: ViewModel(), CoroutineScope, KodeinAware {
    override val kodein = BlackBirdAC.kodein
    override val coroutineContext = newSingleThreadContext("View Model")

    private val jobs = hashMapOf<String, Job>()

    private val _globalProgress = MutableLiveData<Boolean>() withDefault false
    val globalProgress: LiveData<Boolean> get() = _globalProgress


    /**
     * Starts (launches) 'work' block in background with its tag
     * If another work with that tag already exists, the job will be handled according to strategy
     * @param key ('undefined' by default)
     * @param strategy ('cancel' strategy by default)
     * @param work
     */
    fun startJob(
            key: String = "undefined",
            strategy: JobStrategy = JobStrategy.CANCEL,
            work: suspend ()->Unit
    ) = (
            if (jobs.containsKey(key)) // if job with the same tag already exists, behave according to strategy
                doWith(jobs[key], strategy) { launch { work() } }
            else // if there is no job with this tag, create it
                launch { work() }
            )
            .apply {
                indicateStart() // Indicate start of this job and plan end
                invokeOnCompletion { indicateEnd() }
                jobs[key] = this // assign created job (in if-else expr.) to given tag
            }


    private fun indicateStart() = _globalProgress.postValue(true)
    private fun indicateEnd() {
        if (jobs.all { it.value.isCompleted }) {
            // indicate global end only if every job was completed
            _globalProgress.postValue(false)
        }
    }


    override fun onCleared() {
        jobs.forEach { it.value.cancel() }
    }
}