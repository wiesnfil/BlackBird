package com.cvut.blackbird.flows.authentication

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cvut.blackbird.extensions.asProgressTo
import com.cvut.blackbird.extensions.passTo
import com.cvut.blackbird.extensions.withDefault
import com.cvut.blackbird.flows.BlackBirdVM
import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.NotYet
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.services.auth.InitToken
import com.cvut.blackbird.model.services.auth.RefreshTokenGuarded
import com.cvut.blackbird.model.services.kos.LoadCourses
import com.cvut.blackbird.model.services.kos.LoadEvents
import com.cvut.blackbird.model.services.kos.LoadTeachers
import com.cvut.blackbird.model.services.kos.LoadUser
import com.cvut.blackbird.model.support.AuthResult
import org.kodein.di.generic.instance

class AuthViewModel : BlackBirdVM() {

    private val initTokenMS by instance<InitToken>()
    private val refreshTokenGuardedMS by instance<RefreshTokenGuarded>()
    private val loadUserMS by instance<LoadUser>()
    private val loadEventsMS by instance<LoadEvents>()
    private val loadTeachersMS by instance<LoadTeachers>()
    private val loadCoursesMS by instance<LoadCourses>()

    //Output
    private val _authStatus = MutableLiveData<AuthResult>() withDefault AuthResult.NOT_YET
    private val _authLoadingStatus = MutableLiveData<Boolean>() withDefault false
    private val _userInitResult = MutableLiveData<Result<Unit>>() withDefault NotYet()
    private val _userInitState = MutableLiveData<String>() withDefault "Starting login process"

    val authStatus: LiveData<AuthResult> get() = _authStatus
    val authLoadingStatus: LiveData<Boolean> get() = _authLoadingStatus
    val userInitResult: LiveData<Result<Unit>> get() = _userInitResult
    val userInitState: LiveData<String> get() = _userInitState

    init {
        globalProgress passTo _authLoadingStatus
    }

    fun initToken(code: String) {
        startJob("token") {
            val token = initTokenMS(code)
            _authStatus.postValue(token)
        }
    }

    fun refreshToken() {
        startJob("refresh") {
            _authStatus.postValue(refreshTokenGuardedMS())
        }
    }

    fun initUser() { startJob("init") {
        _userInitResult asProgressTo {
            _userInitState.postValue("Looking up your account")
            val user = loadUserMS()

            val events = if (user is Success) {
                _userInitState.postValue("Downloading your timetable")
                loadEventsMS()
            } else return@asProgressTo user as Failure

            val teachers = if (events is Success) {
                _userInitState.postValue("Downloading all your teachers")
                loadTeachersMS()
            } else return@asProgressTo events as Failure

            val courses = if (teachers is Success) {
                _userInitState.postValue("Downloading your courses")
                loadCoursesMS()
            } else return@asProgressTo teachers as Failure

            courses
        }
    } }
}