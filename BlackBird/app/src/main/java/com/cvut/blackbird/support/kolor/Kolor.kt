package com.cvut.blackbird.support.kolor

import android.graphics.Color

class Kolor(colorInt: Int) : Color() {

    companion object {
        fun fromHex(string: String) = Kolor(parseColor(string))
        fun fromSeed(seed: Int): Kolor {
            val shuffled = seed.toString()
                    .toCharArray()
            shuffled.reverse()
            val color = StringBuilder(String.format("#%x", shuffled.contentHashCode()))

            while (color.length < 7) color.append("0")
            return fromHex(color.toString().substring(0, 7))
        }
    }

    private val hsvColor = HSVKolor.fromColorInt(colorInt)

    var saturation
        get() = hsvColor.saturation
        set(value) { hsvColor.saturation = value}

    var brightness
        get() = hsvColor.brightness
        set(value) { hsvColor.brightness = value}


    val colorInt get() = Color.HSVToColor(hsvColor.toArray())

    fun withBrightness(b: Float): Kolor = apply { brightness = b }
    fun withSaturation(s: Float): Kolor = apply { saturation = s }

    data class HSVKolor (var hue: Float, var saturation: Float, var brightness: Float) {
        companion object {
            fun fromColorInt(color: Int): HSVKolor {
                val hsvColor = FloatArray(3)
                Color.colorToHSV(color, hsvColor)
                return HSVKolor(
                        hsvColor[0],
                        hsvColor[1],
                        hsvColor[2]
                )
            }
        }

        fun toArray() = FloatArray(3) {
            when(it){
                0 -> hue
                1 -> saturation
                2 -> brightness
                else -> 0f
            }
        }
    }
}