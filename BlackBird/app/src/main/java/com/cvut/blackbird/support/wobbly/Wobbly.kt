package com.cvut.blackbird.support.wobbly

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.AccelerateInterpolator
import android.widget.*
import androidx.core.view.doOnLayout
import com.cvut.blackbird.extensions.centerX
import com.cvut.blackbird.extensions.centerY
import com.cvut.blackbird.extensions.doOnGlobalLayout
import com.cvut.blackbird.extensions.dpToPx
import com.github.florent37.kotlin.pleaseanimate.please
import org.jetbrains.anko.childrenSequence
import kotlin.math.abs

class Wobbly: ScrollView {
    companion object {
        const val SCROLL_THRESHOLD = 1f
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    private val body = object : LinearLayout(context) {
        override fun onTouchEvent(event: MotionEvent?) = bodyTouchEvent(event)
        override fun onInterceptTouchEvent(event: MotionEvent?) = bodyTouchIntercept(event)
    }
    private val headerScroll = ObservableScrollView(context)
    private val headersLayout = LinearLayout(context)
    private val elementsLayout = FrameLayout(context)

    private var headers = ArrayList<View>()
    private var leads = ArrayList<Pair<Int, View>>()
    private var lineDescriptors = ArrayList<WobblyLineDescriptor>()
    private var elementXScroll = 0

    /**
     * Adapter from which data will be fetched
     */
    var adapter: WobblyAdapter<*>? = null
        set(value) {
            field = value
            initialize()
        }

    /**
     * Initializes all functionality
     * Called on adapter set
     */
    private fun initialize() {
        isFillViewport = true
        isVerticalScrollBarEnabled = false
        body.orientation = LinearLayout.VERTICAL

        setupBody()
        setupHeaders()
        setupElements()
    }

    /**
     * Setups body - host for all elements and header horizontal scrollview
     */
    private fun setupBody() {
        body.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT) // Width | Height
        addView(body)

        headerScroll.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        body.addView(headerScroll)

        body.addView(elementsLayout)
    }

    @SuppressLint("ClickableViewAccessibility")
    /**
     * Sets up header views and header horizontal scrollview.
     * Ensures leads move corresponding to their headers resulting in movement of the whole 'string'
     */
    private fun setupHeaders() {

        headersLayout.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT)
        headersLayout.orientation = LinearLayout.HORIZONTAL

        headers.clear() // clear headers that could have stayed after reset
        for (i in 0 until adapter!!.getColumnCount()) { //create, save and add all headers available in adapter
            val header = adapter!!.getHeader(i)
            headers.add(header)
            headersLayout.addView(header)
        }

        headerScroll.addView(headersLayout)

        headerScroll.onScrollListener = object : ObservableScrollView.OnScrollListener {
            override fun onScrollChanged(scrollView: ObservableScrollView, x: Int, y: Int, oldX: Int, oldY: Int) {
                elementXScroll = x
                leads.forEach {(index, lead) ->
                    (lead as WobblyElement).moveTo( // on horizontal scroll move every lead(first element in vertical line) according to its header
                            headers[index].x
                                    + (headers[index].width/2)
                                    - lead.width/2
                                    - x)
                }
            }
            override fun onEndScroll(scrollView: ObservableScrollView) = Unit
        }
    }


    private var lastX = 0f
    private var startX = 0f
    private var scrollingX = false
    fun bodyTouchIntercept(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN ->  {
                lastX = event.rawX
                startX = lastX
                scrollingX = false
            }
            MotionEvent.ACTION_MOVE -> {
                if (abs(startX - lastX) > SCROLL_THRESHOLD && !scrollingX) {
                    this.requestDisallowInterceptTouchEvent(true)
                    scrollingX = true
                }
                lastX = event.rawX
            }
        }

        return scrollingX
    }
    fun bodyTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                lastX = event.rawX
                startX = lastX
                scrollingX = false
            }
            MotionEvent.ACTION_MOVE -> {
                if (abs(startX - lastX) > SCROLL_THRESHOLD && !scrollingX) {
                    scrollingX = true
                    this.requestDisallowInterceptTouchEvent(true)
                }
                if (scrollingX) {
                    headerScroll.scrollBy(-(event.rawX - lastX).toInt(), 0)
                }
                lastX = event.rawX
            }
        }
        return true
    }

    private fun setupElements() {
        val localAdapter = adapter!! //setup local adapter so that adapter change wont disrupt setup in middle
        var maxY = 0f

        for (column in 0 until localAdapter.getColumnCount()) {
            val items = localAdapter.getItemCount(column)
            val header = headers[column]
            var lastItem: WobblyElement? = null
            var lastY = 0f

            for (i in 0 until items) {
                val element = localAdapter.getItemView(column, i)
                if(i == 0) leads.add(Pair(column, element)) // save first element as lead
                lastItem?.child = element // if there is last element, set current element as its child
                lastItem = element
                val localSpacing = localAdapter.spacingBefore(i, column)

                element.initialize(localAdapter.getItem(i, column)) //let user initialize view passing its data to it

                element.doOnLayout { // when element is ready set its y position and starting x position (x according to its header)
                    element.y = lastY + localSpacing
                    lastY += element.measuredHeight.toFloat() + localSpacing
                    element.x = header.x + (header.width/2) - element.width/2 - elementXScroll
                    if (lastY > maxY) maxY = lastY
                }

                element.tag = WobblyElement.TAG + ":$column:$i" // set custom tag so it can be find
                elementsLayout.addView(element)
            }
        }

        doOnGlobalLayout {
            elementsLayout.layoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,maxY.toInt())
        }
    }

    public fun addLineDescription(before: WobblyElement, withText: String) {
        val (column, row) = positionOf(before)
        if (row == 0) {
            lineDescriptors.add(
                    WobblyLineDescriptor(context, before, headers[column])
                            .withText(withText)
            )
        } else {
            val after = findView(column, row - 1)
            if (after != null) {
                lineDescriptors.add(
                        WobblyLineDescriptor(context, before, after)
                                .withText(withText)
                )
            }
        }
        elementsLayout.addView(lineDescriptors.last())
    }

    public fun addLineDescription(column: Int, before: Int, withText: String) {
        val beforeEle = findView(column, before)
        if (beforeEle != null)
            addLineDescription(beforeEle, withText)
    }

    public fun clearLineDescriptors() = lineDescriptors.clear()

    private val stringPathPaint = Paint().apply {
        isAntiAlias = true
        color = Color.WHITE
        strokeWidth = 3f.dpToPx
    }
    private lateinit var lineCheckpoint: WobblyElement
    override fun onDraw(canvas: Canvas?) {
        for (i in 0 until leads.size) {

            canvas?.drawLine(
                    headers[leads[i].first].centerX - headerScroll.scrollX,
                    headers[leads[i].first].centerY,
                    leads[i].second.centerX,
                    leads[i].second.centerY + headerScroll.height,
                    stringPathPaint)

            lineCheckpoint = leads[i].second as WobblyElement
            while (lineCheckpoint.child != null) {
                canvas?.drawLine(
                        lineCheckpoint.centerX,
                        lineCheckpoint.centerY + headerScroll.height,
                        lineCheckpoint.child!!.centerX,
                        lineCheckpoint.child!!.centerY + headerScroll.height,
                        stringPathPaint)

                lineCheckpoint = lineCheckpoint.child!!
            }
        }

        for (i in 0 until lineDescriptors.size) lineDescriptors[i].updatePosition()

        super.onDraw(canvas)
        invalidate()
    }


    private val lineOutAnimator = ValueAnimator.ofInt(255,0).apply {
        duration = 25
        addUpdateListener { stringPathPaint.alpha = it.animatedValue as Int }
    }
    private val lineInAnimator = ValueAnimator.ofInt(0,255).apply {
        duration = 25
        startDelay = 25
        addUpdateListener { stringPathPaint.alpha = it.animatedValue as Int }
    }
    fun fadeRefresh(refreshAction:() -> Unit) {
        please(50) {
            animate(elementsLayout).toBe { invisible() }
            withStartAction { _ ->
                lineOutAnimator.start()
            }
        }.thenCouldYou(50) {
            animate(elementsLayout).toBe { visible() }
            withStartAction {
                refreshElements()
                refreshAction()
                lineInAnimator.start()
            }
        }.start()
    }


    fun clear() {
        elementsLayout.childrenSequence().forEach {
            ViewAnimationUtils.createCircularReveal(it,it.centerX.toInt(),it.centerY.toInt(), it.width.toFloat(), 0f)
                    .apply {
                        duration = 500
                        interpolator = AccelerateInterpolator()
                    }
                    .start()
        }
        elementsLayout.removeAllViews()
        lineDescriptors.forEach { body.removeView(it) }
        clearLineDescriptors()
        leads.clear()
    }

    fun refreshElements() {
        clear()
        if (adapter != null) setupElements()
    }

    fun findView(column: Int, row: Int): WobblyElement? {
        return elementsLayout.childrenSequence()
                .filter { it is WobblyElement }
                .filter {
                    try { val indices = it.tag.toString().split(':')
                        column == indices[1].toInt()
                                && row == indices[2].toInt() }
                    catch (e: Exception) {
                        Log.e("Wobbly", "Tag: ${it.tag}\nDo not change tags of elements. They are used for locating them.\n${e.message}")
                        false }
                }.firstOrNull() as? WobblyElement
    }

    private fun positionOf(element: WobblyElement): Pair<Int, Int> {
        try { val indices = element.tag.toString().split(':')
            val column = indices[1].toInt()
            val row = indices[2].toInt()
            return Pair(column, row)
        } catch (e: Exception) {
            throw Exception("Tag: ${element.tag}\nDo not change tags of elements. They are used for locating them.\n${e.message}")
        }
    }

    fun wobblyElements() = elementsLayout
            .childrenSequence()
            .toList()
            .filter { it.tag?.toString()?.startsWith(WobblyElement.TAG) ?: false }

    
    var wobblyScrollX
        get() = headerScroll.scrollX
        set(value) { headerScroll.scrollX = value }
    var wobblyScrollY
        get() = this.scrollY
        set(value) { this.scrollY = value }

    fun setScrollInitialState(coords: Pair<Int, Int>) {
        post {
            wobblyScrollX = coords.first
            wobblyScrollY = coords.second
        }
    }
}