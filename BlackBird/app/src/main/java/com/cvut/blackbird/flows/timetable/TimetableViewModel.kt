package com.cvut.blackbird.flows.timetable

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.cvut.blackbird.extensions.atEndOfTheDay
import com.cvut.blackbird.extensions.atStartOfTheDay
import com.cvut.blackbird.extensions.withDefault
import com.cvut.blackbird.flows.BlackBirdVM
import com.cvut.blackbird.model.support.NotYet
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.database.EventDao
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.services.kos.LoadEvents
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.android.Main
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.DateTimeConstants
import org.kodein.di.generic.instance

class TimetableViewModel : BlackBirdVM() {
    private val eventDao by instance<EventDao>()
    private val loadEventsMS by instance<LoadEvents>()

    private val _loadingStatus = MutableLiveData<Boolean>() withDefault false
    private val _timetableUpdateRequestResult = MutableLiveData<Result<List<Event>>>() withDefault NotYet()
    private val _timetable = MutableLiveData<Map<Int,List<Event>>>() withDefault mapOf()
    private val _displayedWeek = MutableLiveData<DateTime>() withDefault DateTime.now()

    val loadingStatus: LiveData<Boolean> get() = _loadingStatus
    val timetableUpdateResult: LiveData<Result<List<Event>>> get() = _timetableUpdateRequestResult
    val timetable: LiveData<Map<Int,List<Event>>> get() = _timetable
    val displayedWeek: LiveData<DateTime> get() = _displayedWeek

    val scrollState = MutableLiveData<Pair<Int, Int>>() withDefault Pair(0, 0)

    init {
        if (_displayedWeek.value!!.dayOfWeek > 5) _displayedWeek.apply { value = value?.plusWeeks(1) }
        _displayedWeek.observeForever { date ->
            requestTimetable(
                    date.withDayOfWeek(DateTimeConstants.MONDAY)
                            .atStartOfTheDay().millis,
                    date.withDayOfWeek(DateTimeConstants.SUNDAY)
                            .atEndOfTheDay().millis
            )
        }
    }

    /**
     * INPUT
     */

    private var requestJob: Job? = null
    private var timetableData: LiveData<List<Event>>? = null
    private val timetableObserver: Observer<List<Event>> = Observer {
        val newValue = it.groupBy {event -> event.startsAt.dayOfWeek }
        if(_timetable.value?.equals(newValue) == false)
            _timetable.postValue(it.groupBy {event -> event.startsAt.dayOfWeek })
    }
    private fun requestTimetable(from: Long, to: Long) {
        requestJob?.cancel()
        requestJob = launch(Dispatchers.Main) {
            timetableData?.removeObserver(timetableObserver)
            timetableData = eventDao.timeSpan(from, to)
            timetableData?.observeForever(timetableObserver)
        }
    }

    private var refreshJob: Job? = null
    fun refresh() {
        refreshJob?.cancel()
        refreshJob = launch { loadEventsMS() }
    }

    fun nextWeek() { _displayedWeek.apply { value = value!!.plusWeeks(1) } }
    fun previousWeek() { _displayedWeek.apply { value = value!!.minusWeeks(1) } }
}