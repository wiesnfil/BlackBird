package com.cvut.blackbird.support.glue

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations


/**
 * Glue structure
 */

class SuperGlue<T>(val life: LifecycleOwner, var emmiter: LiveData<T>)


/**
 * Initializers
 */

infix fun<T> LifecycleOwner.bind(data: LiveData<T>)
        = SuperGlue(this, data)


/**
 * Helper functions
 */

inline infix fun <T, R> LiveData<T>.map(crossinline function: (T) -> R): LiveData<R> =
        Transformations.map(this) { function(it) }

inline infix fun <T, R> LiveData<T>.switchMap(crossinline function: (T) -> LiveData<R>): LiveData<R> =
        Transformations.switchMap(this) { function(it) }

/**
 * Glue functionality
 */

inline infix fun<T> SuperGlue<T>.toPassValueTo(crossinline action: (T) -> Unit) = this.apply {
    emmiter.observe(life, Observer { action(it) })
}

inline infix fun<T> SuperGlue<T>.toNotify(crossinline action: () -> Unit) = this.apply {
    emmiter.observe(life, Observer { action() })
}

inline infix fun SuperGlue<Boolean>.toNotifyOnTrue(crossinline action: () -> Unit) = this.apply {
    emmiter.observe(life, Observer { if(it) action() })
}

inline infix fun <T, R> SuperGlue<T>.withMap(crossinline mapFun: (T) -> R): SuperGlue<R> =
        SuperGlue(life, emmiter.map(mapFun))