package com.cvut.blackbird.model.services

import com.cvut.blackbird.BlackBirdAC
import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.NotYet
import com.cvut.blackbird.model.support.Result
import com.cvut.blackbird.model.support.Success
import retrofit2.Call
import java.net.HttpURLConnection

abstract class BBMicroService {
    protected abstract val authService: AuthService

    suspend fun refreshToken(): Result<Unit> {
        BlackBirdAC.log("Refreshing token")
        return try {
            val response = authService.refreshToken().execute()
            if (response.isSuccessful && response.body() != null) {
                AuthInfo.accessToken = response.body()!!.token
                BlackBirdAC.log("Token refreshed")
                Success(Unit)
            } else {
                Failure(response.errorBody()?.string()
                        ?: "No error message")
            }
        } catch (e: Throwable) {
            Failure(e.localizedMessage)
        }
    }

    suspend inline fun <T> fetch(call: () -> Call<T>): Result<T> {
        var result: Result<T> = NotYet()

        try {
            while (result is NotYet) { // repeat until we have result
                val response = call().execute()
                result = if (response.isSuccessful && response.body() != null)
                // if request was successful (code 200 - 299) and body is not empty
                    Success(response.body()!!)
                else {
                    if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED
                            && refreshToken() is Success)
                    // if request was rejected because of bad AuthToken but token refresh was successful
                    // repeat request
                        NotYet()
                    else
                        Failure(response.errorBody()?.string()
                                ?: "No error message")
                }
            }
        } catch (e: Throwable) {
            return Failure(e.localizedMessage)
        }

        return result
    }
}

fun List<Result<*>>.evaluateResult(): Result<Unit> =
        if(any { it is Failure })
            Failure((first { it is Failure } as Failure).message)
        else Success(Unit)