package com.cvut.blackbird.widget

import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.widget.RemoteViews
import androidx.room.Room
import com.cvut.blackbird.R
import com.cvut.blackbird.extensions.courseAbbr
import com.cvut.blackbird.extensions.millisToTimeText
import com.cvut.blackbird.model.database.BlackBirdDB
import com.cvut.blackbird.model.entities.EventType
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.jetbrains.anko.toast
import org.joda.time.DateTime


/**
 * Implementation of App Widget functionality.
 */
class NextEventWidget : AppWidgetProvider() {
    private var pendingIntent: PendingIntent? = null

    /** STATIC */
    companion object {
        private const val updateInterval: Long = 1000 * 60

        internal fun updateAppWidget(
                context: Context,
                appWidgetManager: AppWidgetManager,
                appWidgetId: Int,
                database: BlackBirdDB
        ) {
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.next_event_widget)

            val event = runBlocking { GlobalScope.async { database.eventDao().nextEvent() }.await() }

            context.toast("Widget ${event?.courseEnt?.code} updated")
            event?.let {
                val time = (it.event.startsAt.millis - DateTime.now().millis)
                views.setTextViewText(R.id.widget_timeLeft, time.millisToTimeText())
                views.setTextViewText(R.id.widget_courseIcon, it.courseEnt?.code?.courseAbbr)
                views.setInt(R.id.widget_courseIcon, "setBackgroundResource",
                        when(it.event.eventType) {
                            EventType.LECTURE  -> R.drawable.ic_timetable_lecture
                            EventType.TUTORIAL -> R.drawable.ic_timetable_tutorial
                            else -> R.drawable.ic_timetable_info
                        })
            }

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }


         /** HELPER FUNCTIONS */
        private fun createPending(context: Context) = Intent(
                context, NextEventService::class.java
        ).let {
            PendingIntent.getService(context, 0, it, PendingIntent.FLAG_CANCEL_CURRENT)
        }
    }


    /** OVERRIDDEN METHODS */

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        val db: BlackBirdDB = Room
                .databaseBuilder(context, BlackBirdDB::class.java, "blackbird_DB")
                .fallbackToDestructiveMigration()
                .build()

        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds)
            updateAppWidget(context, appWidgetManager, appWidgetId, db)
    }

    override fun onEnabled(context: Context) {
        if (pendingIntent == null) pendingIntent = createPending(context)
        val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarm.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), updateInterval, pendingIntent)
    }

    override fun onDisabled(context: Context) {
        val alarm = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarm.cancel(pendingIntent ?: createPending(context))
    }
}

