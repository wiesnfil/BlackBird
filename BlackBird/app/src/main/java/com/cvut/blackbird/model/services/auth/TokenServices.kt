package com.cvut.blackbird.model.services.auth

import com.cvut.blackbird.model.support.Failure
import com.cvut.blackbird.model.support.Success
import com.cvut.blackbird.model.services.AuthInfo
import com.cvut.blackbird.model.services.AuthService
import com.cvut.blackbird.model.services.BBMicroService
import com.cvut.blackbird.model.support.AuthResult

class InitToken(
        override val authService: AuthService
) : BBMicroService() {

    suspend operator fun invoke(code: String): AuthResult {
        val result: AuthResult
        try {
            val response = authService.getToken(code).execute()
            if (response.body()?.token != null && response.body()?.refreshToken != null) {
                AuthInfo.accessToken = response.body()!!.token
                AuthInfo.refreshToken = response.body()!!.refreshToken!!
                result = AuthResult.SUCCESS
            } else
                result = AuthResult.resolveReturnJson(response.errorBody()?.string())
        } catch (e: Throwable) {
            return AuthResult.UNEXPECTED_ERROR.apply {
                errorDesc = "${e.message}\n${e.localizedMessage}"
            }
        }
        return result
    }
}

class RefreshTokenGuarded(
        override val authService: AuthService
) : BBMicroService() {

    suspend operator fun invoke(): AuthResult {
        val returnVal: AuthResult
        try {
            val result = refreshToken()
            returnVal = when (result) {
                is Success -> AuthResult.SUCCESS.setDesc("Successfully refreshed access token")
                is Failure -> AuthResult.resolveReturnJson(
                        result.message,
                        "Failed in refreshing access token\n" +
                                "Message: ${result.message}\n"
                )
                else -> AuthResult.UNEXPECTED_ERROR
            }
        } catch (e: Throwable) {
            return AuthResult.UNEXPECTED_ERROR.apply {
                errorDesc = "${e.message}\n${e.localizedMessage}"
            }
        }
        return returnVal
    }
}