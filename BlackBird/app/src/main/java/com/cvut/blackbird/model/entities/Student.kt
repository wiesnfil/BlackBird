package com.cvut.blackbird.model.entities

import com.tickaroo.tikxml.annotation.Path
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml
import org.joda.time.DateTime

@Xml(name = "atom:content")
class Student {
    @Path("atom:content")
    @PropertyElement(name = "firstName")
    var firstName: String? = null

    @Path("atom:content")
    @PropertyElement(name = "lastName")
    var surname: String? = null

    @Path("atom:content")
    @PropertyElement(name = "email")
    var email: String? = null

    @Path("atom:content")
    @PropertyElement(name = "username")
    var username: String? = null

    @Path("atom:content")
    @PropertyElement(name = "faculty")
    var faculty: String? = null

    @Path("atom:content")
    @PropertyElement(name = "programme")
    var programme: String? = null

    @Path("atom:content")
    @PropertyElement(name = "studyGroup")
    var studyGroup: Int? = null


    @Path("atom:content")
    @PropertyElement(name = "startDate")
    var startDate: String? = null
}