package com.cvut.blackbird

import android.app.Application
import android.util.Log
import com.chibatching.kotpref.Kotpref
import com.cvut.blackbird.dinjection.setupDI
import net.danlew.android.joda.JodaTimeAndroid
import org.kodein.di.Kodein


class BlackBirdAC: Application() {
    companion object {
        const val LOG_TAG = "BLACK_BIRD"
        lateinit var kodein: Kodein

        fun log(message: String) = Log.i(LOG_TAG, message)
    }

    override fun onCreate() {
        super.onCreate()
        //        FragmentManager.enableDebugLogging(true)
        kodein = setupDI()

        JodaTimeAndroid.init(this)
        Kotpref.init(this)
    }
}
