package com.cvut.blackbird.model.entities

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import com.cvut.blackbird.model.database.TeacherDao

class DetailedEvent (
        @Embedded
        val event: Event
) {
    companion object {
        fun empty() = DetailedEvent(Event.empty)
    }

    @Relation(parentColumn = "course", entityColumn = "code")
    var course: Set<Course> = setOf()

    @Relation(parentColumn = "id", entityColumn = "eventId")
    var userNotes: List<EventNote> = listOf()
    val userNote get() = userNotes.firstOrNull() ?: EventNote(event.id)

    val courseEnt: Course? get() = course.firstOrNull()

    @Ignore var teacherList: List<Teacher> = listOf()

    @Ignore
    suspend fun initTeachers(teacherDao: TeacherDao) {
        if (event.linked?.teachers?.isEmpty() == true) return
        teacherList = teacherDao.getTeachers(event.linked!!.teachers!!)
    }



    // Override methods

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DetailedEvent

        if (event != other.event) return false
        if (course != other.course) return false
        if (userNotes != other.userNotes) return false
        if (teacherList != other.teacherList) return false

        return true
    }

    override fun hashCode(): Int {
        var result = event.hashCode()
        result = 31 * result + course.hashCode()
        result = 31 * result + userNotes.hashCode()
        result = 31 * result + teacherList.hashCode()
        return result
    }


}