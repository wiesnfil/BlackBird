package com.cvut.blackbird.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.graphics.Color
import android.os.Parcel
import android.os.Parcelable
import android.view.View
import android.view.ViewAnimationUtils
import androidx.core.animation.addListener
import androidx.interpolator.view.animation.FastOutSlowInInterpolator


class RevealPosition(private val view: View) {
    public var x = view.centerX
    public var y = view.centerY

    fun top() {y = view.y}
    fun down() {y = view.y + view.height}
    fun left() {x = view.x}
    fun right() {x = view.x + view.width}
}

fun View.centerCircularReveal(duration: Long? = null) = revealFrom(centerX.toInt(), centerY.toInt(), duration)

fun View.revealFrom(xPos: Int, yPos: Int, duration: Long? = null) {
    ViewAnimationUtils.createCircularReveal(this, xPos, yPos, 0f, width.toFloat())
            .apply { if (duration != null) this.duration = duration }
            .start()
}

inline fun View.revealFrom(duration: Long? = null, f:RevealPosition.() -> Unit) {
    val pos = RevealPosition(this)
    pos.f()
    revealFrom(pos.x.toInt(), pos.y.toInt(), duration)
}


/**
 * Transition Reveal
 */

class CircularRevealSettings(
        val x: Int, val y: Int,
        val width: Int, val height: Int,
        val startColor: Int = Color.TRANSPARENT,
        val endColor: Int = Color.TRANSPARENT
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(x)
        parcel.writeInt(y)
        parcel.writeInt(width)
        parcel.writeInt(height)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<CircularRevealSettings> {
        override fun createFromParcel(parcel: Parcel): CircularRevealSettings {
            return CircularRevealSettings(parcel)
        }

        override fun newArray(size: Int): Array<CircularRevealSettings?> {
            return arrayOfNulls(size)
        }
    }
}

fun View.registerCircularReveal(settings: CircularRevealSettings) =
        doOnLayoutChange {
            val duration = 1000L

            val finalRadius = Math.sqrt(
                    (settings.width * settings.width + settings.height * settings.height)
                            .toDouble()
            ).toFloat()

            val anim: Animator = ViewAnimationUtils.createCircularReveal(this,
                    settings.x,
                    settings.y,
                    0f,
                    finalRadius
            ).setDuration(duration)

            anim.interpolator = FastOutSlowInInterpolator()
            anim.start()
            startColorAnimation(settings.startColor, settings.endColor, duration)
        }
fun View.startCircularExit(settings: CircularRevealSettings, onFinish: ()->Unit) {
    val duration = 500L

    val initRadius = Math.sqrt(
            (settings.width * settings.width + settings.height * settings.height)
                    .toDouble()
    ).toFloat()

    val anim: Animator = ViewAnimationUtils.createCircularReveal(this,
            settings.x,
            settings.y,
            initRadius,
            0f
    ).setDuration(duration)

    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator?) {
            onFinish()
        }
    })

    anim.interpolator = FastOutSlowInInterpolator()
    anim.start()
    startColorAnimation(settings.startColor, settings.endColor, duration)
}



fun View.startColorAnimation(startColor: Int, endColor: Int, duration: Long) {
    val anim = ValueAnimator()
    anim.setIntValues(startColor, endColor)
    anim.setEvaluator(ArgbEvaluator())
    anim.addUpdateListener { valueAnimator -> setBackgroundColor(valueAnimator.animatedValue as Int) }
    anim.duration = duration
    anim.start()
}