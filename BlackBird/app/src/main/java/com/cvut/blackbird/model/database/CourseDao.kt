package com.cvut.blackbird.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cvut.blackbird.model.entities.Course

@Dao
interface CourseDao {

    @Query("SELECT * FROM Course WHERE code = :code")
    fun getCourse(code: String): Course?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(courses: List<Course>)

    @Query("DELETE FROM Course")
    fun deleteAll()
}