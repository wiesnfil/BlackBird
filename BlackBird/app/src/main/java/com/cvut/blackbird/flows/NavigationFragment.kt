package com.cvut.blackbird.flows

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.cvut.blackbird.R
import kotlinx.android.synthetic.main.navigation_fragment.*

class NavigationFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.navigation_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        NavigationUI.setupWithNavController(bottom_navigation, findBottomNav()!!)
    }
}

fun Fragment.findMainNav(): NavController? = activity?.findNavController(R.id.navHost)
fun Fragment.findBottomNav(): NavController? = activity?.findNavController(R.id.bottomNavHost)
