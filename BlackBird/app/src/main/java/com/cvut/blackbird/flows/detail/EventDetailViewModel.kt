package com.cvut.blackbird.flows.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cvut.blackbird.extensions.hasNoValue
import com.cvut.blackbird.extensions.withDefault
import com.cvut.blackbird.flows.BlackBirdVM
import com.cvut.blackbird.model.database.EventDao
import com.cvut.blackbird.model.database.TeacherDao
import com.cvut.blackbird.model.entities.DetailedEvent
import com.cvut.blackbird.model.entities.Event
import org.kodein.di.generic.instance

class EventDetailViewModel: BlackBirdVM() {
    private val eventDao by instance<EventDao>()
    private val teacherDao by instance<TeacherDao>()

    /**
     * Observables
     */
    private val _detailedEvent = MutableLiveData<DetailedEvent>() withDefault DetailedEvent.empty()

    val detailedEvent: LiveData<DetailedEvent> get() = _detailedEvent
    var eventInitialized = false
        private set



    /**
     * INPUT
     */

    fun setEvent(event: Event) {
        startJob("set") {
            _detailedEvent.postValue(eventDao.getDetailedEvent(event.id).apply {
                initTeachers(teacherDao)
            })
            eventInitialized = true
        }
    }


    fun changeEventNote(text: String) {
        startJob("userNote") {
            if (detailedEvent.hasNoValue() && _detailedEvent.value!!.event == Event.empty) return@startJob

            val userNote = detailedEvent.value!!.userNote
            eventDao.saveEventNote(userNote.apply {
                note = text
            })
        }
    }
}