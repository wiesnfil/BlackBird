package com.cvut.blackbird.support.glue

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer


/**
 * Glue structure
 */

class Glue(val life: LifecycleOwner, val actor: View)


/**
 * Initializers
 */

infix fun LifecycleOwner.bind(view: View) = Glue(this, view)


/**
 * Helper functions
 */

inline fun<T> LifecycleOwner.observe(data: LiveData<T>, crossinline stalker: (p: T) -> Unit) =
        data.observe(this, Observer { stalker(it) })


/**
 * Glue functionality
 */

//STATE

infix fun Glue.visibilityTo(source: LiveData<Boolean>) {
    source.observe(life, Observer { actor.visibility =
            if(it) View.VISIBLE
            else View.INVISIBLE
    }) }

infix fun Glue.enabledTo(source: LiveData<Boolean>) {
    source.observe(life, Observer { actor.isEnabled = it })
}


//ACTION

infix fun Glue.clickTo(receiver: () -> Unit) = this.apply {
    actor.setOnClickListener { receiver.invoke() }
}
inline fun<T> Glue.clickTo(crossinline source: () -> T, crossinline receiver: (T) -> Unit) = this.apply {
    actor.setOnClickListener { receiver.invoke(source()) }
}

infix fun <T> Glue.clickToInvoke(receiver: () -> T) = this.apply {
    actor.setOnClickListener { receiver.invoke() }
}
inline fun<T, R> Glue.clickToInvoke(crossinline source: () -> T, crossinline receiver: (T) -> R) = this.apply {
    actor.setOnClickListener { receiver.invoke(source()) }
}