package com.cvut.blackbird.model.entities

import com.cvut.blackbird.model.Constants
import com.cvut.blackbird.model.services.UserInfo
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Path
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml
import java.security.InvalidKeyException

@Xml
class NewsRoot(
        @Path("channel")
        @Element(name = "item")
        val news: List<News>
) { companion object {
        fun getNewsLink(): String =
                if (false) {
                        when (UserInfo.facultyAbbr) {
                                Constants.FACULTY_FEL -> "https://www.fel.cvut.cz/aktuality/rss.xml"
                                Constants.FACULTY_FIT -> "https://fit.cvut.cz/rss-novinky.xml"
                                else -> throw InvalidKeyException("Faculty ${UserInfo.facultyAbbr} is not supported!")
                        }
                } else {
                        when (UserInfo.facultyAbbr) {
                                Constants.FACULTY_FEL -> "https://www.fel.cvut.cz/en/aktuality/rss-en.xml"
                                Constants.FACULTY_FIT -> "https://fit.cvut.cz/rss-news.xml"
                                else -> throw InvalidKeyException("Faculty ${UserInfo.facultyAbbr} is not supported!")
                        }
                }

} }

@Xml
class News(
        @PropertyElement
        val title: String,

        @PropertyElement
        val description: String,

        @PropertyElement
        val category: String,

        @PropertyElement
        val link: String
)