package com.cvut.blackbird.support.wobbly

import android.view.View

interface WobblyAdapter<T> {

    fun getColumnCount(): Int

    fun getItem(position: Int, column: Int = 0): T

    fun getItemView(position: Int, column: Int = 0): WobblyElement

    fun getItemCount(column: Int = 0): Int

    fun getHeader(column: Int): View

    fun spacingBefore(position: Int, column: Int): Float = 0f
}