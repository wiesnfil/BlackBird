package com.cvut.blackbird.model.entities

import androidx.room.*
import com.cvut.blackbird.model.services.EventsMeta
import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime

data class EventParent(
        @SerializedName("events") val events: List<Event>?
)

@Entity(indices = [Index("eventTypeRaw")])
data class Event(
        @PrimaryKey
        @SerializedName("id")
        val id: Int,

        @Embedded
        @SerializedName("note")
        val note: Note?,

        @SerializedName("sequence_number")
        val sequenceNum: Int,

        @SerializedName("starts_at")
        val startsAt: DateTime,

        @SerializedName("ends_at")
        val endsAt: DateTime,

        @SerializedName("deleted")
        val deleted: Boolean,

        @SerializedName("capacity")
        val capacity: Int,

        @SerializedName("occupied")
        val occupied: Int,

        @SerializedName("event_type")
        val eventTypeRaw: String,

        @SerializedName("parallel")
        val parallel: String,

        @Embedded
        @SerializedName("links")
        val linked: Links?
) {
    companion object {
        val empty = Event(-1,null,0, DateTime.now(), DateTime.now(), true, 0,0,"", "", null)
    }
    val eventType: EventType get() = EventType.parseLowerCase(eventTypeRaw)
    var isPinned: Boolean
        get() = EventsMeta.pinned.contains(id.toString())
        set(value) {
            if (value) EventsMeta.pinned.add(id.toString())
            else EventsMeta.pinned.remove(id.toString())
        }
}


data class Links(

        @SerializedName("course")
        val course: String?,

        @SerializedName("room")
        val room: String?,

        @SerializedName("teachers")
        val teachers: List<String>?
)

data class Note(

        @SerializedName("cs")
        val cs: String?,

        @SerializedName("en")
        val en: String?
)

enum class EventType(val cs: String, val en: String) {
    EXAM("Zkouška","ExamTask"),
    LECTURE("Přednáška","Lecture"),
    TUTORIAL("Cvičení","Tutorial"),
    UNDEFINED("Nedefinovaný","Undefined");

    companion object {
        fun parseLowerCase(string: String): EventType {
            return EventType.values()
                    .find {
                        it.name.toLowerCase() == string
                    } ?: UNDEFINED
        }
    }
}