package com.cvut.blackbird.flows.timetable

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.SparseArray
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter

import com.cvut.blackbird.R
import kotlinx.android.synthetic.main.timetable_fragment.*
import org.joda.time.DateTime
import org.joda.time.DateTimeConstants

class TimetableFragment : Fragment() {

    companion object {
        fun newInstance(): TimetableFragment {
            return TimetableFragment()
        }
    }
    private lateinit var viewModel: TimetableViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.timetable_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(TimetableViewModel::class.java)
        updateDate(viewModel.date)

        setupUI()
        setupBinding()
    }

    private fun setupUI() {
        setupViewPager()
        timetableTabLayout.setupWithViewPager(timetableViewPager)

        if(viewModel.date.dayOfWeek > 5) timetableTabLayout.getTabAt(0)?.select()
        else timetableTabLayout.getTabAt(viewModel.date.dayOfWeek - 1)?.select()

        leftArrowBtn.setOnClickListener { updateDate(viewModel.date.minusWeeks(1)) }
        rightarrowBtn.setOnClickListener { updateDate(viewModel.date.plusWeeks(1)) }
    }

    private fun setupBinding() {
    }

    private fun updateDate(date: DateTime) {
        viewModel.requestTimetable(
                date.withDayOfWeek(DateTimeConstants.MONDAY)
                        .withHourOfDay(0).millis,
                date.withDayOfWeek(DateTimeConstants.SUNDAY)
                        .withHourOfDay(23).millis
        )
        viewModel.date = date
        weekDateTxt.text = date.monthOfYear().asText + " ${date.withDayOfWeek(1).dayOfMonth} - ${date.withDayOfWeek(5).dayOfMonth}"
    }


//Day Fragments setup

    private fun setupViewPager() {
        timetableViewPager.adapter = object : FragmentPagerAdapter(childFragmentManager) {
            private val days = arrayOf("Monday", "Tuesday", "Wednesday", "Thursday", "Friday")

            override fun getItem(position: Int): Fragment = TimetableDayFragment.newInstance(position + 1)
            override fun getCount(): Int = 5
            override fun getPageTitle(position: Int): CharSequence? = days[position]
        }
    }
}

