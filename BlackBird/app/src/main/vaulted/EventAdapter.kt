package com.cvut.blackbird.flows.tasks

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cvut.blackbird.R
import com.cvut.blackbird.support.kolor.Kolor
import com.cvut.blackbird.extensions.lastIndexOfNumber
import com.cvut.blackbird.model.entities.Event
import com.cvut.blackbird.model.entities.EventType
import kotlinx.android.synthetic.main.exam_list_row.view.*
import kotlinx.android.synthetic.main.lecture_list_row.view.*
import org.joda.time.DateTime
import org.joda.time.Days
import org.joda.time.Minutes

class EventListAdapter(private val clickListener: (Event) -> Unit): ListAdapter<Event, EventListAdapter.EventViewHolder>(
        object : DiffUtil.ItemCallback<Event>() {
            override fun areItemsTheSame(oldItem: Event, newItem: Event) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Event, newItem: Event) = oldItem == newItem
        }
) {
    class EventViewHolder(val view: View) : RecyclerView.ViewHolder(view){

        fun bind(event: Event, clickListener: (Event) -> Unit) {
            val roomTxt: TextView
            val courseAbbrTxt: TextView

            when (event.eventType) {
                EventType.EXAM -> {
                    roomTxt = view.examRoomTxt
                    view.examLengthTxt.text = "${Minutes.minutesBetween(event.startsAt, event.endsAt).minutes} minutes"
                    courseAbbrTxt = view.examCourseAbbr
                    view.examOccupiedTxt.text = event.occupied.toString()
                    view.examCapacityTxt.text = event.capacity.toString()
                    if (event.note != null)
                        view.examInfoTxt.text = buildString {
                            append(event.note.cs)
                            if (event.note.en != null) append(event.note.en)
                            view.examInfoTxt.visibility = View.VISIBLE
                        }
                    view.examDueTimeTxt.text = "in ${Days.daysBetween(DateTime.now(),event.startsAt).days} days"
                }
                else -> {
                    roomTxt = view.lectureRoomTxt
                    courseAbbrTxt = view.lectureCourseAbbr
                    view.lectureNumTxt.text = event.sequenceNum.toString()
                    view.lectureDueTimeTxt.text = event.startsAt.toString("HH:mm")
                }
            }
            roomTxt.text = event.linked?.room

            if(event.linked?.course != null && event.linked.course.length >= 3) {
                val course = event.linked.course
                courseAbbrTxt.text = course.substring(course.lastIndexOfNumber(), course.length)

                courseAbbrTxt.backgroundTintList = ColorStateList.valueOf(Kolor
                        .fromSeed(course.hashCode())
                        .withBrightness(0.75f)
                        .withSaturation(0.65f)
                        .colorInt)
            }

            view.setOnClickListener{clickListener(event)}
        }
    }

    override fun getItemViewType(position: Int): Int {
        return EventType.parseLowerCase(getItem(position).eventTypeRaw).ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val layout = when(viewType) {
            EventType.EXAM.ordinal -> R.layout.exam_list_row
            else -> R.layout.lecture_list_row
        }
        return EventViewHolder(LayoutInflater.from(parent.context)
                .inflate(layout, parent, false))
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }
}