package com.cvut.blackbird.flows.timetable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.cvut.blackbird.R
import com.cvut.blackbird.flows.zupport.EventListAdapter
import com.cvut.blackbird.model.entities.Event
import kotlinx.android.synthetic.main.timetable_day_fragment.*
import org.jetbrains.anko.toast
import org.joda.time.DateTime

class TimetableDayFragment : Fragment() {

    companion object {
        fun newInstance(day: Int): TimetableDayFragment {
            return TimetableDayFragment().setDay(day)
        }
    }

    private lateinit var viewModel: TimetableViewModel
    private var day: Int? = null
    private var listAdapter: EventListAdapter = EventListAdapter {
        activity?.toast("Clicked on ${it.linked?.course}")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.timetable_day_fragment, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(TimetableViewModel::class.java)
        setupUi()
        setupBinding(day ?: 7)
    }

    private fun setupUi() {
        timetableDayRecuclerView.adapter = listAdapter
        timetableDayRecuclerView.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
    }

    private fun setupBinding(day: Int) {
        viewModel.timetable.observe(this, Observer {
            val events = it ?: arrayListOf()
            if(events.isEmpty()) {
                listAdapter.submitList(arrayListOf())
                checkEventCount(events)
            }
            else {
                val date = events[0].startsAt
                val filtered = events.filter { it in date.withDayOfWeek(day) }
                listAdapter.submitList(filtered)
                checkEventCount(filtered)
            }
        })
    }

    private fun checkEventCount(events: List<Event>) {
        if (events.isEmpty())
            this.timetableDayInfo?.text = "No events found for this day"
        else
            this.timetableDayInfo?.text = ""
    }

    //Helper functions
    private fun setDay(day: Int): TimetableDayFragment {
        this.day = day
        return this
    }
}

private operator fun DateTime.contains(event: Event): Boolean {
    return dayOfWeek() == DateTime(event.startsAt).dayOfWeek()
}
